#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <utils/config/Config.h>
#include <utils/definitions.h>
#include <utils/logging/Logger.h>
#include <image/IAlgorithm.h>
#include <image/ImageMatcher.h>
#include <video/IAlgorithm.h>
#include <video/Full.h>
#include <video/Basic.h>
#include <video/Quick.h>
#include <image/hashing/TL_ImageHash.h>
using namespace eurofins;
using namespace eurofins::utils;
int main() {
	auto cfg = Config::loadSettings("./resources/cfg/videocomparison.cfg");
	//std::cout << cfg->getValueFromKey(std::string("imagematching"), std::string("")) << std::endl;
	//std::cout << "config made tree test: " << *(cfg->getSequenceStringTreeFromKey(std::string("imagematching"))) << std::endl;
	//AlgorithmFactory* dummyFactory = new DummyFactory();
	//auto sequence = config->getSequenceFromKey<AlgoI>("imagematching", dummyFactory);
	DEB("Settings successfully loaded");

	Logger logger;

	cv::VideoCapture refCap;
	cv::VideoCapture testCap;
	refCap.open(cfg->getValueFromKey<std::string>("refstream", ""));
	testCap.open(cfg->getValueFromKey<std::string>("teststream", ""));

	if (!(refCap.isOpened() && testCap.isOpened())) {
		DEB("Unable to open streams.");
		system("PAUSE");
		return -1;
	}
	DEB("Streams successfully opened.");

	image::ImageMatcher imgMatcher(*cfg);
	video::IAlgorithm* videoMatching = new video::Full;

	TIMEBLOCK("FullVideoMatching execution")
	{
		videoMatching->execute(refCap, testCap, imgMatcher, logger);
	}
	logger.outputCLI();
	cv::waitKey(0);
	system("PAUSE");
	return 0;
}