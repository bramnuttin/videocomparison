#include "KeyFrameExtractor.h"
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
}
namespace eurofins {
namespace video {
std::shared_ptr<std::vector<int>> KeyFrameExtractor::getKeyFramesFromHashedStream(const std::vector<int>& stream)
{
	//ph_dct_imagehash(nullptr, NULL);
	std::shared_ptr<std::vector<int>> extracted(new std::vector<int>(stream));
	float hashingSimilarityThreshold = 0.30;
	int current = -1;
	for (int x : stream) {
		if ((std::abs(x / current) - 1) <= hashingSimilarityThreshold) {
			extracted->push_back(x);
			current = x;
		}
	}
	return extracted;
}

std::shared_ptr<std::vector<cv::Mat>> KeyFrameExtractor::getKeyFramesFromVideoStream(cv::VideoCapture& stream, image::ImageMatcher& matcher)
{
	/*const char    *url = "./resources/streams/drop_limited.mp4";
	AVFormatContext *s = NULL;
	int ret = avformat_open_input(&s, url, NULL, NULL);
	if (ret < 0)
		abort();*/
		/*std::cout << "Trying avcodec_register_all... " << std::endl;
		avcodec_register_all();
		std::cout << "Done.\n" << std::endl;
		av_register_all();
		AVFormatContext *pFormatCtx = NULL;
		cv::InputArray test(std::vector<int>);
		//NULL -> autodetect
		if (avformat_open_input(&pFormatCtx, "./resources/streams/drop_limited.mp4", NULL, NULL) != 0) {
			std::cout << "Input not found!" << std::endl;
			return nullptr;
		}
		if (avformat_find_stream_info(pFormatCtx, NULL) < 0) {
			std::cout << "No streams found" << std::endl;
			return nullptr;
		}
		// Dump information about file onto standard error
		av_dump_format(pFormatCtx, 0, "./resources/streams/drop_limited.mp4", 0);

		//AVCodecContext *pCodecCtxOrig = NULL;
		AVCodecParameters *pCodecParams = NULL;
		AVCodecContext *pCodecCtxOrig = NULL;
		AVCodecContext *pCodecCtx = NULL;

		// Find the first video stream
		int videoStream = -1;
		for (int i = 0; i < pFormatCtx->nb_streams; i++) {
			if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
				videoStream = i;
				break;
			}
		}
		if (videoStream == -1) {
			std::cout << "Didn't find a video stream" << std::endl;
			return nullptr; // Didn't find a video stream
		}
		// Get a pointer to the codec context for the video stream
		pCodecParams = pFormatCtx->streams[videoStream]->codecpar;
		avcodec_parameters_to_context(pCodecCtx, pCodecParams); // error memory access denied!
		AVCodec *pCodec = NULL;

		// Find the decoder for the video stream
		pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
		if (pCodec == NULL) {
			fprintf(stderr, "Unsupported codec!\n");
			return nullptr; // Codec not found
		}
		// Copy context

		pCodecCtx = avcodec_alloc_context3(pCodec);
		if (avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0) {
			fprintf(stderr, "Couldn't copy codec context");
			return nullptr; // Error copying codec context
		}
		// Open codec
		if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
			return nullptr; // Could not open codec

		system("PAUSE");
		//avformat_close_input(&s);
		*/
	std::shared_ptr<std::vector<cv::Mat>> extracted = std::make_shared<std::vector<cv::Mat>>();
	utils::Logger uselessLogger;
	cv::Mat m;
	cv::Mat current;
	stream.read(current);
	extracted->push_back(current);
	unsigned int count = 0;
	//stream.read(m);
	//extracted->push_back(m);
	cv::Mat tempCurrent;
	cv::Mat tempM;
	//std::cout << ph_about() << std::endl;
	while (stream.grab()) {
		stream.retrieve(m);
		tempCurrent = current;
		tempM = m;
		if (!matcher.matchImages(tempCurrent, tempM, count++, count, uselessLogger)) {
			DEB("keyframe found!");
			m.copyTo(current);
			extracted->push_back(current.clone());
		}
		DEB_IMG(tempCurrent, tempM);
	}
	stream.set(CV_CAP_PROP_POS_FRAMES, 0);
	return extracted;
}
} // namespace video
} // namespace eurofins