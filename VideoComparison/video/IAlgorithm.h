#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "../image/ImageMatcher.h"
#include "../utils/definitions.h"

namespace eurofins {
namespace video {
class IAlgorithm
{
public:
	virtual ~IAlgorithm() {}
	virtual float execute(cv::VideoCapture& refCap, cv::VideoCapture& testCap, image::ImageMatcher& imgMatcher, utils::Logger& logger) {
		DEB("This execute method (VideoCapture) is not supported");
		return -1;
	}
	virtual float execute(std::vector<cv::Mat>& refCap, std::vector<cv::Mat>& testCap, image::ImageMatcher& imgMatcher, utils::Logger& logger) {
		DEB("This execute method (vector<Mat>) is not supported");
		return -1;
	}
	virtual float execute(std::vector<int>& refHashes, std::vector<int>& testHashes, utils::Logger& logger) {
		DEB("This execute method (vector<int>) is not supported");
		return -1;
	}
	//virtual void execute();
};
} // namespace video
} // namespace eurofins