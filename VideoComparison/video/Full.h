#pragma once
#include <vector>
#include "IAlgorithm.h"
#include "../utils/logging/Logger.h"

namespace eurofins {
namespace video {
class Full : public IAlgorithm
{
public:
	virtual float execute(cv::VideoCapture& refCap, cv::VideoCapture& testCap, image::ImageMatcher& imgMatcher, utils::Logger& logger);
	virtual float execute(std::vector<cv::Mat>& refCap, std::vector<cv::Mat>& testCap, image::ImageMatcher& imgMatcher, utils::Logger& logger);
};
} // namespace video
} // namespace eurofins