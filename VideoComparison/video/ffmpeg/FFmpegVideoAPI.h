#pragma once
#include <string>
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/pixfmt.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

namespace eurofins {
namespace video {
class FFmpegVideoAPI
{
public:
	FFmpegVideoAPI() : pImgConvertCtx(NULL), baseTime(0.0),
		fps(0.0), isOpen(false), streamIndex(-1),
		pCodec(NULL), pCodecCtx(NULL),
		pFormatCtx(NULL) {
		;
	}
	~FFmpegVideoAPI();

	bool openFile(std::string& filepath);
	bool closeFile();
	AVFrame* getNextFrame();
	AVFrame * getNextKeyFrame();
	AVCodecContext *pCodecCtx = NULL;
private:
	AVCodec *pCodec = NULL;
	AVFormatContext *pFormatCtx = NULL;

	AVCodecParameters *pCodecParams = NULL;
	bool isOpen = false;
	int streamIndex;
	double fps;
	double baseTime;
	struct SwsContext *pImgConvertCtx;

	int width;
	int height;

	bool openVideo();
	void closeVideo();
	AVFrame* getRGBAFrame(AVFrame* pFrameYuv);
	bool decode(const AVPacket* avpkt, AVFrame* pOutFrame);
};
} // namespace video
} // namespace eurofins
