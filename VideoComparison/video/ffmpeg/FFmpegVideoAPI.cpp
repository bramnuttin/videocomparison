#include "FFmpegVideoAPI.h"
#include <iostream>

namespace eurofins {
namespace video {
FFmpegVideoAPI::~FFmpegVideoAPI()
{
}

bool FFmpegVideoAPI::openFile(std::string & filepath)
{
	closeFile();

	av_register_all();

	// Open media file.
	if (avformat_open_input(&pFormatCtx, filepath.c_str(), NULL, NULL) != 0)
	{
		closeFile();
		return false;
	}

	// Get format info.
	if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		closeFile();
		return false;
	}
	av_dump_format(pFormatCtx, 0, filepath.c_str(), 0);
	// open video and audio stream.
	bool hasVideo = openVideo();

	if (!hasVideo)
	{
		closeFile();
		return false;
	}

	isOpen = true;

	// Get file information.
	if (streamIndex != -1)
	{
		fps = av_q2d(pFormatCtx->streams[streamIndex]->r_frame_rate);
		// Need for convert time to ffmpeg time.
		baseTime = av_q2d(pFormatCtx->streams[streamIndex]->time_base);
	}

	return true;
}

bool FFmpegVideoAPI::closeFile()
{
	isOpen = false;

	// Close video and audio.
	closeVideo();

	if (pFormatCtx)
	{
		avformat_close_input(&pFormatCtx);
		pFormatCtx = NULL;
	}

	return true;
}

AVFrame * FFmpegVideoAPI::getNextFrame()
{
	AVFrame * res = NULL;

	if (streamIndex != -1)
	{
		AVFrame *pVideoYuv = av_frame_alloc();
		AVPacket packet;

		if (isOpen)
		{
			// Read packet.
			while (av_read_frame(pFormatCtx, &packet) >= 0)
			{
				int64_t pts = 0;
				pts = (packet.dts != AV_NOPTS_VALUE) ? packet.dts : 0;

				if (packet.stream_index == streamIndex)
				{
					// Convert ffmpeg frame timestamp to real frame number.
					int64_t numberFrame = (double)((int64_t)pts -
						pFormatCtx->streams[streamIndex]->start_time) *
						baseTime * fps;

					// Decode frame
					bool isDecodeComplite = decode(&packet, pVideoYuv);
					if (isDecodeComplite)
					{
						res = getRGBAFrame(pVideoYuv);
					}
					break;
				}
				av_packet_unref(&packet);

				packet = AVPacket();
			}

			av_free(pVideoYuv);
		}
	}

	return res;
}

AVFrame * FFmpegVideoAPI::getNextKeyFrame()
{
	AVFrame * res = NULL;

	if (streamIndex != -1)
	{
		AVFrame *pVideoYuv = av_frame_alloc();
		AVPacket packet;

		if (isOpen)
		{
			// Read packet.
			/*double m_out_start_time = 0.0;
			int flgs = AVSEEK_FLAG_ANY;
			int seek_ts = (m_out_start_time*(pCodecCtx->time_base.den)) / (pCodecCtx->time_base.num);
			if (av_seek_frame(pFormatCtx, streamIndex, seek_ts, flgs) < 0)
			{
				return nullptr;
			}*/
			//av_seek_frame(pFormatCtx, streamIndex, 0.9, AVSEEK_FLAG_ANY);
			while (av_read_frame(pFormatCtx, &packet) >= 0)
			{
				int64_t pts = 0;
				pts = (packet.dts != AV_NOPTS_VALUE) ? packet.dts : 0;
				if (packet.stream_index == streamIndex)
				{
					// Convert ffmpeg frame timestamp to real frame number.
					int64_t numberFrame = (double)((int64_t)pts -
						pFormatCtx->streams[streamIndex]->start_time) *
						baseTime * fps;
					//std::cout << "numberFrame: " << numberFrame << std::endl;
					//std::cout << pFormatCtx->streams[streamIndex]->time_base.num << "/" << pFormatCtx->streams[streamIndex]->time_base.den << std::endl;
					//std::cout << "duration: " << pFormatCtx->duration << ", " << packet.dts << ", " << packet.pts << std::endl;
					//av_dump_format(pFormatCtx, NULL, 0, NULL);
					pFormatCtx->streams[streamIndex]->skip_to_keyframe = 1; // WILL IT WORK?? YES IT WILL!!!
					//std::cout << "-------" << std::endl;
					//	if (av_seek_frame(pFormatCtx, streamIndex, numberFrame, AVSEEK_FLAG_BACKWARD) < 0)
						//	return nullptr;
					//std::cout << "-------" << std::endl;
					//av_dump_format(pFormatCtx, NULL, 0, NULL);
					//std::cout << "numberFrame: " << numberFrame << std::endl;
					//std::cout << pFormatCtx->streams[streamIndex]->time_base.num << "/" << pFormatCtx->streams[streamIndex]->time_base.den << std::endl;
					//std::cout << "duration: " << pFormatCtx->duration << ", " << packet.dts << ", " << packet.pts << std::endl;
					// Louche
					/*int64_t timeBase;
					timeBase = (int64_t(pCodecCtx->time_base.num) * AV_TIME_BASE) / int64_t(pCodecCtx->time_base.den);
					int64_t seekTarget = int64_t(numberFrame) * timeBase;
					std::cout << "seekTarget: " << seekTarget << ", index: " << numberFrame << ", basetime: " << baseTime << ", fps: " << fps << std::endl;

					baseTime += 0.005;*/
					// End Louche
					// Louche2

					// End Louche2

					// Decode frame
					bool isDecodeComplite = decode(&packet, pVideoYuv);
					if (isDecodeComplite)
					{
						res = getRGBAFrame(pVideoYuv);
					}
					break;
				}
				av_packet_unref(&packet);

				packet = AVPacket();
			}

			av_free(pVideoYuv);
		}
	}

	return res;
}

bool FFmpegVideoAPI::openVideo()
{
	bool res = false;

	if (pFormatCtx)
	{
		streamIndex = -1;

		for (unsigned int i = 0; i < pFormatCtx->nb_streams; i++)
		{
			if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
			{
				streamIndex = i;
				pCodecParams = pFormatCtx->streams[i]->codecpar; // HOPE THIS
				//avcodec_parameters_to_context(pCodecCtx, pCodecParams); // WORKS, IT DOESN'T :'(
				//pCodecCtx = pFormatCtx->streams[i]->codec;
				std::cout << pCodecParams->width << ", " << pCodecParams->bit_rate << std::endl;
				pCodec = avcodec_find_decoder(pCodecParams->codec_id);
				//pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
				pCodecCtx = avcodec_alloc_context3(pCodec);
				avcodec_parameters_to_context(pCodecCtx, pCodecParams);
				if (pCodec)
				{
					res = !(avcodec_open2(pCodecCtx, pCodec, NULL) < 0);
					width = pCodecCtx->coded_width;
					height = pCodecCtx->coded_height;
				}

				break;
			}
		}

		if (!res)
		{
			closeVideo();
		} else
		{
			std::cout << "width: " << pCodecCtx->width << ", height: " << pCodecCtx->height << ", pix_fmt: "
				<< pCodecCtx->pix_fmt << std::endl;
			/*pImgConvertCtx = sws_getContext(pCodecCtx->width, pCodecCtx->height,
				pCodecCtx->pix_fmt,
				pCodecCtx->width, pCodecCtx->height,
				AV_PIX_FMT_BGR24,
				SWS_BICUBIC, NULL, NULL, NULL);*/
			pImgConvertCtx = sws_getContext(pCodecCtx->width, pCodecCtx->height,
				pCodecCtx->pix_fmt,
				pCodecCtx->width, pCodecCtx->height,
				AV_PIX_FMT_BGR24,
				SWS_BICUBIC, NULL, NULL, NULL);
		}
	}

	return res;
}

void FFmpegVideoAPI::closeVideo()
{
	if (pCodecCtx)
	{
		avcodec_close(pCodecCtx);
		pCodecCtx = NULL;
		pCodec = NULL;
		streamIndex = 0;
	}
}

AVFrame * FFmpegVideoAPI::getRGBAFrame(AVFrame * pFrameYuv)
{
	AVFrame * frame = NULL;
	int width = pCodecCtx->width;
	int height = pCodecCtx->height;

	int bufferImgSize = av_image_get_buffer_size(AV_PIX_FMT_BGR24, width, height, 1); // TODO: align might be wrong
	frame = av_frame_alloc();
	uint8_t * buffer = (uint8_t*)av_mallocz(bufferImgSize);
	if (frame)
	{
		av_image_fill_arrays(frame->data, frame->linesize, buffer, AV_PIX_FMT_BGR24, pCodecCtx->width, pCodecCtx->height, 1); // align same as aline in buffer!
		//avpicture_fill((AVPicture*)frame, buffer, AV_PIX_FMT_BGR24, width, height);
		frame->width = width;
		frame->height = height;
		//frame->data[0] = buffer;

		sws_scale(pImgConvertCtx, pFrameYuv->data, pFrameYuv->linesize,
			0, height, frame->data, frame->linesize);
	}
	return (AVFrame *)frame;
}

bool FFmpegVideoAPI::decode(const AVPacket * avpkt, AVFrame * pOutFrame)
{
	bool res = false;

	if (pCodecCtx)
	{
		if (avpkt && pOutFrame)
		{
			int got_picture_ptr = 0;
			int videoFrameBytes = avcodec_decode_video2(pCodecCtx, pOutFrame, &got_picture_ptr, avpkt);

			//			avcodec_decode_video(pVideoCodecCtx, pOutFrame, &videoFrameBytes, pInBuffer, nInbufferSize);
			res = (videoFrameBytes > 0);
		}
	}

	return res;
}
} // namespace video
} // namespace eurofins