#include "FFmpegAdapter.h"

FFmpegAdapter::FFmpegAdapter()
{
}

FFmpegAdapter::~FFmpegAdapter()
{
}

std::shared_ptr<cv::Mat> FFmpegAdapter::AVFrame2Mat(AVFrame& frame, int rows, int columns)
{
	return std::make_shared<cv::Mat>(rows, columns, CV_8UC3, frame.data[0], frame.linesize[0]);
}

std::shared_ptr<AVFrame> FFmpegAdapter::Mat2AVFrame(cv::Mat& mat)
{
	return std::make_shared<AVFrame>();
}