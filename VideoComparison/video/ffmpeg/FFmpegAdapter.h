#pragma once
#include <opencv2/core.hpp>
#include <memory>
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/pixfmt.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

class FFmpegAdapter
{
public:
	FFmpegAdapter();
	~FFmpegAdapter();

	static std::shared_ptr<cv::Mat> AVFrame2Mat(AVFrame& frame, int rows, int columns);
	static std::shared_ptr<AVFrame> Mat2AVFrame(cv::Mat& mat);
};
