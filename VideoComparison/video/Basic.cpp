#include "Basic.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include "../utils/definitions.h"

namespace eurofins {
namespace video {
using utils::Logger;
float Basic::execute(cv::VideoCapture& refCap, cv::VideoCapture& testCap, image::ImageMatcher& imgMatcher, Logger& logger) {
	//TODO implement!!! beter array gebruiken!
	cv::Mat refFrame;
	cv::Mat testFrame;
	unsigned int refCount = 0;
	unsigned int testCount = 0;
	unsigned int totalRefCount = refCap.get(CV_CAP_PROP_FRAME_COUNT);
	unsigned int totalTestCount = testCap.get(CV_CAP_PROP_FRAME_COUNT);
	cv::Mat tempRef; //wss weg of betere methode
	cv::Mat tempTest; //wss weg of betere methode

	for (unsigned int i = 0; i < totalTestCount && i < totalRefCount; i++) {
		refCap.read(refFrame);
		testCap.read(testFrame);
		if (refFrame.size().area() == 0 || testFrame.size().area() == 0) {
			return 1.0f; //TODO now it stoppes if unequal stream lengths!!
		}
		tempRef = refFrame;
		tempTest = testFrame;
		float score = imgMatcher.matchImages(tempRef, tempTest, refCount, testCount, logger);
		DEB_IMG(tempRef, tempTest);
		refCap.set(CV_CAP_PROP_POS_FRAMES, ++refCount);
		testCap.set(CV_CAP_PROP_POS_FRAMES, ++testCount);
	}
	return 1.0f;
}

float Basic::execute(std::vector<cv::Mat>& refCap, std::vector<cv::Mat>& testCap, image::ImageMatcher & imgMatcher, utils::Logger & logger)
{
	for (unsigned int i = 0; i < refCap.size() && i < testCap.size(); i++) {
		imgMatcher.matchImages(refCap.at(i), testCap.at(i), i, i, logger);
		DEB_IMG(refCap.at(i), testCap.at(i));
	}
	return 1.0f;
}
} // namespace video
} // namespace eurofins