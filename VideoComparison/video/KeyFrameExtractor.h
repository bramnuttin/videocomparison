#pragma once
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <memory>
#include "../image/ImageMatcher.h"
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
}
namespace eurofins {
namespace video {
class KeyFrameExtractor
{
public:
	static std::shared_ptr<std::vector<int>> getKeyFramesFromHashedStream(const std::vector<int>&);
	static std::shared_ptr<std::vector<cv::Mat>> getKeyFramesFromVideoStream(cv::VideoCapture&, image::ImageMatcher&);
};
} // namespace video
} // namespace eurofins
