#include "Full.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include "../utils/definitions.h"

namespace eurofins {
namespace video {
using utils::Logger;
float Full::execute(cv::VideoCapture& refCap, cv::VideoCapture& testCap, image::ImageMatcher& imgMatcher, Logger& logger) {
	//TODO implement!!! beter array gebruiken!
	cv::Mat refFrame;
	cv::Mat testFrame;
	unsigned int refCount = 0;
	unsigned int testCount = 0;
	unsigned int totalRefCount = refCap.get(CV_CAP_PROP_FRAME_COUNT);
	unsigned int totalTestCount = testCap.get(CV_CAP_PROP_FRAME_COUNT);
	cv::Mat tempRef; //wss weg of betere methode
	cv::Mat tempTest; //wss weg of betere methode

	for (unsigned int i = 0; i < totalTestCount && i < totalRefCount; i++) {
		refCap.read(refFrame);
		testCap.read(testFrame);
		if (refFrame.size().area() == 0 || testFrame.size().area() == 0) {
			return 1.0f; //TODO now it stoppes if unequal stream lengths!!
		}
		tempRef = refFrame;
		tempTest = testFrame;
		float score = imgMatcher.matchImages(tempRef, tempTest, refCount, testCount, logger);
		DEB_IMG(tempRef, tempTest);
		if (!score) {
			//refCap.set(CV_CAP_PROP_POS_FRAMES, 0); //OPPASSEN!!!
			tempRef = refFrame;
			tempTest = testFrame;
			for (unsigned int i = refCount + 1; i < totalRefCount; i++) {
				tempTest = testFrame; //wss weg of betere methode
				refCap.read(tempRef);

				imgMatcher.matchImages(tempRef, tempTest, i, testCount, logger);
				DEB_IMG(tempRef, tempTest);
			}
			refCap.set(CV_CAP_PROP_POS_FRAMES, refCount);
			//testCap.set(CV_CAP_PROP_POS_FRAMES, 0); //OPPASSEN!!!

			for (unsigned int i = testCount + 1; i < totalTestCount; i++) {
				tempRef = refFrame;//wss weg of betere methode
				testCap.read(tempTest);

				imgMatcher.matchImages(tempRef, tempTest, refCount, i, logger);
				DEB_IMG(tempRef, tempTest);
			}
		}
		refCap.set(CV_CAP_PROP_POS_FRAMES, ++refCount);
		testCap.set(CV_CAP_PROP_POS_FRAMES, ++testCount);
	}
	return 1.0f;
}
float Full::execute(std::vector<cv::Mat>& refCap, std::vector<cv::Mat>& testCap, image::ImageMatcher & imgMatcher, utils::Logger & logger)
{
	for (unsigned int i = 0; i < refCap.size() && i < testCap.size(); i++) {
		float score = imgMatcher.matchImages(refCap.at(i).clone(), testCap.at(i).clone(), i, i, logger);
		if (!score) {
			for (unsigned int j = i + 1; j < refCap.size(); j++) {
				imgMatcher.matchImages(refCap.at(j).clone(), testCap.at(i).clone(), j, i, logger);
			}
			for (unsigned int j = i + 1; j < testCap.size(); j++) {
				imgMatcher.matchImages(refCap.at(i).clone(), testCap.at(j).clone(), i, j, logger);
			}
		}
	}
	return 1.0f;
}
} // namespace video
} // namespace eurofins