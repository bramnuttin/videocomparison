#include "Quick.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include "../utils/definitions.h"
#include "../image/hashing/TL_ImageHash.h"
#include "../image/hashing/TL_EdgeImHash.h"
#include "KeyFrameExtractor.h"
#include "Full.h"
#include "Basic.h"
namespace eurofins {
namespace video {
using utils::Logger;
float Quick::execute(cv::VideoCapture& refCap, cv::VideoCapture& testCap, image::ImageMatcher& imgMatcher, Logger& logger) {
	// TODO: run hashing
	cv::Mat testMat;
	refCap >> testMat;
	TL_EdgeImHash hash(testMat, 16, 16);
	hash.compute(testMat);
	std::cout << "hash total: " << hash.getHash() << std::endl;
	system("PAUSE"); // TODO: weg!!
	//run keyframe extraction
	std::shared_ptr<std::vector<cv::Mat>> refKeys = KeyFrameExtractor::getKeyFramesFromVideoStream(refCap, imgMatcher);
	std::shared_ptr<std::vector<cv::Mat>> testKeys = KeyFrameExtractor::getKeyFramesFromVideoStream(testCap, imgMatcher);

	std::cout << refKeys->size() << ", " << testKeys->size() << std::endl;
	system("PAUSE");
	//execute full algorithm with result
	IAlgorithm* full = new Basic;
	if (refKeys->empty() || testKeys->empty()) {
		return 0.0f;
	}
	return full->execute(*refKeys, *testKeys, imgMatcher, logger);
}

float Quick::execute(std::vector<cv::Mat>& refCap, std::vector<cv::Mat>& testCap, image::ImageMatcher & imgMatcher, utils::Logger & logger)
{
	return 0.0f;
}
} // namespace video
} // namespace eurofins