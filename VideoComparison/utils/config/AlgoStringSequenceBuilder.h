#pragma once
#include <memory>
#include "TreeNode.h"
#include "Config.h"

namespace eurofins {
namespace utils {
class AlgoStringSequenceBuilder
{
public:
	template<typename T>
	static TreeNode<T>* convertTree2T(const TreeNode<AlgoString>* algoStringTree, std::shared_ptr<T>(*factoryMake)(const std::shared_ptr<utils::AlgoString> algoString));
private:
	template<typename T>
	static void resolveTree(const TreeNode<AlgoString>* stringNode, TreeNode<T>* node, std::shared_ptr<T>(*factoryMake)(const std::shared_ptr<utils::AlgoString> algoString));
};

template<typename T>
inline TreeNode<T>* AlgoStringSequenceBuilder::convertTree2T(const TreeNode<AlgoString>* algoStringTree, std::shared_ptr<T>(*factoryMake)(const std::shared_ptr<utils::AlgoString> algoString))
{
	utils::TreeNode<T>* algoTree = new utils::TreeNode<T>(factoryMake(algoStringTree->algo), algoStringTree->threshold);
	for (int i = 0; i < algoStringTree->children->size(); i++) {
		resolveTree<T>(algoStringTree->children->at(i), algoTree, factoryMake);
	}
	return algoTree;
}

template<typename T>
inline void AlgoStringSequenceBuilder::resolveTree(const TreeNode<AlgoString>* stringNode, TreeNode<T>* parent, std::shared_ptr<T>(*factoryMake)(const std::shared_ptr<utils::AlgoString> algoString))
{
	auto current = new utils::TreeNode<T>(factoryMake(stringNode->algo), stringNode->threshold);

	if (!stringNode->children->empty()) {
		for (int i = 0; i < stringNode->children->size(); i++) {
			resolveTree(stringNode->children->at(i), current, factoryMake);
		}
	}
	parent->addChildNode(current);
}
} // namespace utils
} // namespace eurofins