#pragma once
#include <vector>
#include <memory>
#include <ostream>

namespace eurofins {
namespace utils {
template <class T>
class TreeNode
{
public:
	TreeNode(std::shared_ptr<T> algo, const float threshold[2], std::shared_ptr<std::vector<const TreeNode<T>*>> nodes);
	TreeNode(std::shared_ptr<T> algo, const float threshold[2]);
	~TreeNode();

	float threshold[2];
	std::shared_ptr<std::vector<const TreeNode<T>*>> children;
	std::shared_ptr<T> algo;

	void addChildNode(const TreeNode<T>* const node);
	template <class J>
	friend std::ostream& operator<<(std::ostream& os, const TreeNode<J>& tree);
};

template <class T>
TreeNode<T>::TreeNode(std::shared_ptr<T> algo, const float threshold[2]) : algo(algo)
{
	TreeNode::threshold[0] = threshold[0];
	TreeNode::threshold[1] = threshold[1];
	children = std::make_shared<std::vector<const TreeNode<T>*>>();
}

template <class T>
TreeNode<T>::TreeNode(std::shared_ptr<T> algo, const float threshold[2], std::shared_ptr<std::vector<const TreeNode<T>*>> nodes) : algo(algo), children(nodes)
{
	TreeNode::threshold[0] = threshold[0];
	TreeNode::threshold[1] = threshold[1];
}

template<class T>
inline TreeNode<T>::~TreeNode()
{
	if (!children->empty())
		for (unsigned int i = 0; i < children->size(); i++)
			delete (*children)[i];
}

template <class T>
void TreeNode<T>::addChildNode(const TreeNode<T> * const node)
{
	(*children).push_back(node);
}

template <typename T>
inline std::ostream & operator<<(std::ostream& os, const TreeNode<T>& tree)
{
	auto name = (typeid(*tree.algo).name());
	//remove "class "
	name += 6;

	os << name;
	os << '(';
	os << tree.threshold[0];
	os << ',';
	os << tree.threshold[1];
	os << ')';
	auto children = tree.children;
	for (auto &t : *children) {
		os << std::endl;
		os << '[';
		os << *t;
		os << ']';
	}
	return os;
}
} // namespace utils
} // namespace eurofins