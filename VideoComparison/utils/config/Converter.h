#pragma once
#include <string>

namespace eurofins {
namespace utils {
class Converter
{
public:
	template <typename T>
	static T string2T(const std::string& const val)
	{
		std::istringstream istr(val);
		T returnVal;
		if (!(istr >> returnVal))
			throw ConfigException(std::string("Not a valid ") + std::string(typeid(T).name()) + std::string(" received"));
		return returnVal;
	}

	template <>
	static std::string string2T(const std::string& const val)
	{
		return val;
	}
};
} // namespace utils
} // namespace eurofins
