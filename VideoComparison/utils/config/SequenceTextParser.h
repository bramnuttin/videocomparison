#pragma once
#include <string>
#include <memory>
#include <vector>
#include <map>

namespace eurofins {
namespace utils {
class SequenceTextParser
{
public:
	static std::string getCurrentNodeValue(const std::string& seqText);
	static std::shared_ptr<std::vector<std::string>> getChildNodes(const std::string& seqText);
	static std::string getNodeParams(const std::string& node);
	static std::string getFirstChildNode(const std::string& seqText);
	static float* getThresholds(const std::string& text);

	static std::shared_ptr<std::map<std::string, std::string>> getParamsFromString(std::string args);
	static bool isKeyValuePair(const std::string& line);
};
} // namespace utils
} // namespace eurofins