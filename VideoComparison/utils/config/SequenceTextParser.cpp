#include "SequenceTextParser.h"
#include <regex>

namespace eurofins {
namespace utils {
static const std::string ELEMENT_BEGIN = "\\[";
static const std::string ELEMENT_END = "\\]";
static const int ELEMENT_BEGIN_END_LENGTH = 1;

static const std::string PARAM_BEGIN = "\\(";
static const std::string PARAM_END = "\\)";
static const int PARAM_BEGIN_END_LENGTH = 1;

/*
Gets only the first element value, but none of its children
*/
std::string SequenceTextParser::getCurrentNodeValue(const std::string & seqText)
{
	if (seqText.empty()) {
		return seqText;
	}

	std::string nodevalue_regex_string = "[" + ELEMENT_BEGIN + "]";
	nodevalue_regex_string += "(.*?)";
	nodevalue_regex_string += "[" + ELEMENT_END + "|" + ELEMENT_BEGIN + "]";
	std::regex nodevalue_regex(nodevalue_regex_string);

	std::smatch match;
	if (std::regex_search(seqText, match, nodevalue_regex) && match.size() > 1) {
		return match.str(1);
	} else {
		return "";
	}
}

/*
Gets a vector with for each child itself and its children
*/
std::shared_ptr<std::vector<std::string>> SequenceTextParser::getChildNodes(const std::string & seqText)
{
	if (seqText == "")
		return std::shared_ptr<std::vector<std::string>>();
	size_t pS, pE;
	std::shared_ptr<std::vector<std::string>> children = std::make_shared<std::vector<std::string>>();

	//remove element begin and end of root value
	pS = ELEMENT_BEGIN_END_LENGTH;
	pE = seqText.size() - ELEMENT_BEGIN_END_LENGTH - pS;
	auto childrenText = seqText.substr(pS, pE);
	//remove root value
	pS = childrenText.find_first_of(ELEMENT_BEGIN);
	if (pS == childrenText.npos) {
		return children;
	}
	childrenText = childrenText.substr(pS);

	auto child = getFirstChildNode(childrenText);
	std::size_t pointer = 0;
	while (!child.empty()) {
		children->push_back(child);
		pointer = child.size();
		pS = pointer;
		childrenText = childrenText.substr(pS);
		child = getFirstChildNode(childrenText);
	}

	return children;
}

std::string SequenceTextParser::getFirstChildNode(const std::string& seqText)
{
	std::string tempSeq = seqText;
	int children = 0;
	bool end = false;
	size_t pS, pE;
	while (!end) {
		pS = tempSeq.find_first_of(ELEMENT_BEGIN);
		pE = tempSeq.find_first_of(ELEMENT_END);
		if (pS < pE) {
			children++;
			tempSeq = tempSeq.substr(pS + 1);
		} else {
			children--;
			tempSeq = tempSeq.substr(pE + 1);
		}

		if (children <= 0) {
			end = true;
		}
	}

	pS = seqText.find_first_of(ELEMENT_BEGIN);
	if (pS == seqText.npos)
		return seqText;
	std::string returnString = seqText.substr(pS, pS + (seqText.size() - tempSeq.size()));
	return returnString;
}

std::string SequenceTextParser::getNodeParams(const std::string& node)
{
	if (node.empty()) {
		return node;
	}

	std::string nodethresholds_regex_string = "[" + PARAM_BEGIN + "]";
	nodethresholds_regex_string += "(.*?)";
	nodethresholds_regex_string += "[" + PARAM_END + "]";
	std::regex nodethresholds_regex(nodethresholds_regex_string);

	std::smatch match;
	if (std::regex_search(node, match, nodethresholds_regex) && match.size() > 1) {
		return match.str(1);
	} else {
		return "";
	}
}

float* SequenceTextParser::getThresholds(const std::string& text)
{
	float *thresholds = new float;
	auto cpos = text.find_first_of(',');
	thresholds[0] = std::stof(text.substr(0, cpos));
	auto threshold1text = text.substr(cpos + 1, text.size());
	cpos = threshold1text.find_first_of(',');
	thresholds[1] = std::stof(threshold1text.substr(0, cpos));
	return thresholds;
}

std::shared_ptr<std::map<std::string, std::string>> SequenceTextParser::getParamsFromString(std::string args)
{
	std::shared_ptr<std::map<std::string, std::string>> params = std::make_shared<std::map<std::string, std::string>>();
	size_t next = 0;
	std::string param;
	std::string newArgs = args;
	while (next != newArgs.npos) {
		next = newArgs.find_first_of(',');
		param = newArgs.substr(0, next);
		if (isKeyValuePair(param)) {
			auto it = param.find_first_of('=');
			auto key = param.substr(0, it);
			auto value = param.substr(it + 1);
			(*params)[key] = value;
		}
		newArgs = newArgs.substr(next + 1, newArgs.npos);
	}
	return params;
}

bool SequenceTextParser::isKeyValuePair(const std::string &line)
{
	return (line.find_first_of('=') != line.npos);
}
} // namespace utils
} // namespace eurofins