#pragma once
#include <string>
#include <map>
#include <memory>
#include <exception>
#include <iostream>
#include "Converter.h"
#include "TreeNode.h"
#include "../definitions.h"

namespace eurofins {
namespace utils {
struct AlgoString {
public:
	AlgoString() {}
	AlgoString(std::string name) : name(name) {}
	AlgoString(std::string name, std::shared_ptr<std::map<std::string, std::string>> params) : name(name), params(params) {}

	std::string name = "noname";
	std::shared_ptr<std::map<std::string, std::string>> params;
};

class ConfigException : public std::runtime_error
{
public:
	ConfigException(const std::string& message) : runtime_error("CFG: " + message) {}
};

class Config
{
public:
	static const std::string kSequenceStart;
	static const std::string kSequenceEnd;

	Config(std::shared_ptr<std::map<std::string, std::string>>);

	static std::shared_ptr<Config> loadSettings(const std::string& path);
	static std::shared_ptr<Config> loadSettings(std::ifstream& stream);
	template<typename T>
	T getValueFromKey(const std::string& key, const T& defaultValue) const;
	template<typename T>
	TreeNode<T>* getSequenceTreeFromKey(const std::string& key, std::shared_ptr<T>(*factoryMake)(std::string& name, std::shared_ptr<std::map<std::string, std::string>> params));
	TreeNode<AlgoString>* getSequenceStringTreeFromKey(std::string& key);

private:
	const std::shared_ptr<std::map<std::string, std::string>> data_;

	template<typename T>
	TreeNode<T>* fillTree(utils::TreeNode<T>* root, std::string& text, std::shared_ptr<T>(*factoryMake)(std::string& name, std::shared_ptr<std::map<std::string, std::string>> params));
	TreeNode<AlgoString>* fillTree(TreeNode<AlgoString>* root, std::string& text);
	static bool isWhitespace(const std::string &line);
	static bool isKeyValuePair(const std::string &line);
	static void removeComments(std::string &line);
	static void removeWhitespace(std::string &line);
};

template<typename T>
inline T Config::getValueFromKey(const std::string & key, const T & defaultValue) const
{
	auto value = data_->find(key);
	if (value == data_->end())
		return defaultValue;
	return Converter::string2T<T>(value->second);
}

template<typename T>
inline TreeNode<T>* Config::getSequenceTreeFromKey(const std::string & key, std::shared_ptr<T>(*factoryMake)(std::string &name, std::shared_ptr<std::map<std::string, std::string>>params))
{
	DEB("Building sequence ...");
	auto seqText = getValueFromKey<std::string>(key, "");
	float floats[] = { 0.0f, 1.0f };

	// Set root node
	std::string algo = SequenceTextParser::getCurrentNodeValue(seqText);
	auto nodeParams = SequenceTextParser::getNodeParams(algo);
	TreeNode<T>* tree = new TreeNode<T>(factoryMake(algo, SequenceTextParser::getParamsFromString(nodeParams)), floats);

	std::shared_ptr<std::vector<std::string>> children = SequenceTextParser::getChildNodes(seqText);
	for (int i = 0; i < children->size(); i++) {
		fillTree(tree, children->at(i), factoryMake);
	}

	DEB("Tree structure: " << *tree);
	return tree;
}

template<typename T>
inline TreeNode<T>* Config::fillTree(utils::TreeNode<T>* root, std::string & text, std::shared_ptr<T>(*factoryMake)(std::string &name, std::shared_ptr<std::map<std::string, std::string>>params))
{
	float threshold[2] = { 0.0f, 1.0f };
	std::shared_ptr<std::vector<std::string>> children = SequenceTextParser::getChildNodes(text);
	if (children->size() == 0) {
		auto nodeValueWParams = SequenceTextParser::getCurrentNodeValue(text);
		auto nodeParams = SequenceTextParser::getNodeParams(nodeValueWParams);
		//auto nodeValue = nodeValueWThresholds.substr(0, nodeValueWThresholds.size() - nodeThresholds.size());
		root->addChildNode(new TreeNode<T>(factoryMake(nodeValueWParams, SequenceTextParser::getParamsFromString(nodeParams)), threshold));
	} else {
		for (int i = 0; i < children->size(); i++) {
			auto nodeValueWParams = SequenceTextParser::getCurrentNodeValue(text);
			auto nodeParams = SequenceTextParser::getNodeParams(nodeValueWParams);
			//auto nodeValue = nodeValueWThresholds.substr(0, nodeValueWThresholds.size() - nodeThresholds.size());
			root->addChildNode(fillTree(new TreeNode<T>(factoryMake(nodeValueWParams, SequenceTextParser::getParamsFromString(nodeParams)), threshold), children->at(i), factoryMake));
		}
	}

	return root;
}
} // namespace utils
} // namespace eurofins
