#include "Config.h"
#include <fstream>
#include <iostream>
#include "../definitions.h"
#include "SequenceTextParser.h"

namespace eurofins {
namespace utils {
const std::string Config::kSequenceStart = "START";
const std::string Config::kSequenceEnd = "END";

Config::Config(std::shared_ptr<std::map<std::string, std::string>> map) : data_(map) {}

std::shared_ptr<Config> Config::loadSettings(const std::string& path)
{
	std::ifstream in(path);
	if (!in) {
		throw ConfigException("Failed to open configuration file \'" + path + "\'");
	}

	return loadSettings(in);
}

std::shared_ptr<Config> Config::loadSettings(std::ifstream& stream)
{
	if (stream.fail()) {
		throw ConfigException("Configuration stream error");
	}
	std::shared_ptr<std::map<std::string, std::string>> map = std::make_shared<std::map<std::string, std::string>>();
	std::string line;
	while (std::getline(stream, line)) {
		if (!isWhitespace(line)) {
			removeComments(line);
			removeWhitespace(line);
			if (isKeyValuePair(line)) {
				auto it = line.find_first_of('=');
				auto key = line.substr(0, it);
				auto value = line.substr(it + 1);
				(*map)[key] = value;
			} else if (line.find_first_of(kSequenceStart) != line.npos) {
				auto key = line.substr(line.find(kSequenceStart) + kSequenceStart.length());
				std::string value = "";
				while (std::getline(stream, line) && line.find_first_of(kSequenceEnd)) {
					value.append(line);
				}
				(*map)[key] = value;
			}
		}
	}
	return std::make_shared<Config>(map);
}

TreeNode<AlgoString>* Config::getSequenceStringTreeFromKey(std::string& key)
{
	DEB("Building sequence ...");
	auto seqText = getValueFromKey<std::string>(key, "");
	float floats[] = { 0.0f, 1.0f };

	// Set root node
	std::string algo = SequenceTextParser::getCurrentNodeValue(seqText);
	auto nodeParams = SequenceTextParser::getNodeParams(algo);
	TreeNode<AlgoString>* tree = new TreeNode<AlgoString>(std::make_shared<AlgoString>(algo, SequenceTextParser::getParamsFromString(nodeParams)), floats);

	std::shared_ptr<std::vector<std::string>> children = SequenceTextParser::getChildNodes(seqText);
	for (int i = 0; i < children->size(); i++) {
		fillTree(tree, children->at(i));
	}

	DEB("Tree structure: " << *tree);
	return tree;
}

TreeNode<AlgoString> * Config::fillTree(TreeNode<AlgoString>* root, std::string& text)
{
	float threshold[2] = { 0.0f, 1.0f };
	std::shared_ptr<std::vector<std::string>> children = SequenceTextParser::getChildNodes(text);
	if (children->size() == 0) {
		auto nodeValueWParams = SequenceTextParser::getCurrentNodeValue(text);
		auto nodeParams = SequenceTextParser::getNodeParams(nodeValueWParams);
		//auto nodeValue = nodeValueWThresholds.substr(0, nodeValueWThresholds.size() - nodeThresholds.size());
		root->addChildNode(new TreeNode<AlgoString>(std::make_shared<AlgoString>(nodeValueWParams, SequenceTextParser::getParamsFromString(nodeParams)), SequenceTextParser::getThresholds(nodeParams)));
	} else {
		for (int i = 0; i < children->size(); i++) {
			auto nodeValueWParams = SequenceTextParser::getCurrentNodeValue(text);
			auto nodeParams = SequenceTextParser::getNodeParams(nodeValueWParams);
			//auto nodeValue = nodeValueWThresholds.substr(0, nodeValueWThresholds.size() - nodeThresholds.size());
			root->addChildNode(fillTree(new TreeNode<AlgoString>(std::make_shared<AlgoString>(nodeValueWParams, SequenceTextParser::getParamsFromString(nodeParams)), SequenceTextParser::getThresholds(nodeParams)), children->at(i)));
		}
	}

	return root;
}

bool Config::isWhitespace(const std::string& line)
{
	return (line.find_first_not_of(' ') == line.npos);
}

bool Config::isKeyValuePair(const std::string& line)
{
	return (line.find_first_of('=') != line.npos);
}

void Config::removeComments(std::string& line)
{
	if (line.find("##") != line.npos)
		line.erase(line.find("##"));
}

void Config::removeWhitespace(std::string& line)
{
	auto beginSpace = line.find_first_of(' ');
	auto endSpace = line.find_first_not_of(' ', beginSpace);
	std::string fill = "";
	while (beginSpace != std::string::npos) {
		auto range = endSpace - beginSpace;

		line.replace(beginSpace, range, fill);

		auto newStart = beginSpace + fill.length();
		beginSpace = line.find_first_of(' ', newStart);
		endSpace = line.find_first_not_of(' ', beginSpace);
	}
}
} // namespace utils
} // namespace eurofins