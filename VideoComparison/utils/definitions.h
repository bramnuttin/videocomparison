#pragma once
#include <iostream>
#include <ctime>

#define DEBUG 1
#if DEBUG
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define DEB(x) std::cout << __FILENAME__ << "(" << __LINE__ << "): " << x << std::endl
#else
#define DEB(x)
#endif

#define DEBUG_TIMER 1
#if DEBUG_TIMER
#define TIME clock()
#define TIME_LOG(text,x) std::cout << text << ": " << x << std::endl
#define TIMEBLOCK(data_)   for (long blockTime = NULL; (blockTime == NULL ? (blockTime = clock()) != NULL : false); TIME_LOG(data_ << " (ms)", (double) (clock() - blockTime) / CLOCKS_PER_SEC))
#else
#define TIME 0
#define TIME_LOG(text,x)
#define TIMEBLOCK(data_)
#endif

#define DEBUG_IMAGES 1
#if DEBUG_IMAGES
#define DEB_IMG(r,t) cv::imshow("Reference Frame", r); cv::imshow("Test Frame", t); cv::waitKey(4)
#else
#define DEB_IMG(r,t)
#endif