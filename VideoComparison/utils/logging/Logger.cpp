#include "Logger.h"
#include <iostream>
#include <queue>

namespace eurofins {
namespace utils {
Logger::Logger()
{
	//auto cmp = [](int left, int right) { return (left ^ 1) < (right ^ 1); };
	//std::priority_queue<int, std::vector<int>, decltype(cmp)> q3(cmp);
	logs_ = std::make_unique<std::vector<LoggingInfo>>();
	//std::vector<std::vector<LoggingInfo>> newLogs = std::vector<std::vector<LoggingInfo>>();
}

Logger::~Logger()
{
}

void Logger::log(LoggingInfo& info) {
	logs_->push_back(info);
	if (info.refFrameN > maxRefFrameN_) {
		maxRefFrameN_ = info.refFrameN;
	}
	if (info.testFrameN > maxTestFrameN_) {
		maxTestFrameN_ = info.testFrameN;
	}
}

void Logger::outputAsXML() {
	processLogs();
	//see TL_ENGINE
}

void Logger::outputCLI() {
	/*this->logs->clear();
	this->maxFrameN = 0;
	log(LoggingInfo(0, 0, Log::black));
	log(LoggingInfo(0, 1, Log::match));
	log(LoggingInfo(0, 2, Log::nomatch));
	log(LoggingInfo(0, 3, Log::match));
	log(LoggingInfo(1, 0, Log::black));
	log(LoggingInfo(2, 0, Log::black));
	log(LoggingInfo(3, 0, Log::black));
	log(LoggingInfo(1, 1, Log::nomatch));
	log(LoggingInfo(1, 2, Log::match));
	log(LoggingInfo(1, 3, Log::nomatch));
	log(LoggingInfo(2, 1, Log::nomatch));
	log(LoggingInfo(3, 1, Log::match));
	log(LoggingInfo(2, 2, Log::nomatch));
	log(LoggingInfo(2, 3, Log::nomatch));
	log(LoggingInfo(3, 2, Log::nomatch));
	log(LoggingInfo(3, 3, Log::match));
	*/
	auto groups_ptr = processLogs();
	auto groups = *groups_ptr;
	for (unsigned int i = 0; i < groups.size(); i++) {
		auto group = groups.at(i);
		if (group != nullptr) {
			std::cout << "Log(" << group->refFrameNBegin << ", " << group->refFrameNEnd << ")(" << group->testFrameNBegin << ", " << group->testFrameNEnd << ") [" << std::endl;
			std::cout << "log = " << (int)group->log << std::endl;
			//std::cout << "similarity = " << log.similarity << std::endl;
			std::cout << "]" << std::endl;
		}
	}
	/*for (unsigned int i = 0; i < logs->size(); i++) {
	auto log = logs->at(i);
	std::cout << "Log(" << log.refFrameN << ", " << log.testFrameN << ") [" << std::endl;
	std::cout << "log = " << (int) log.log << std::endl;
	std::cout << "similarity = " << log.similarity  << std::endl;
	std::cout << "]" << std::endl;
	}
	*/
}

std::vector<GroupedLoggingInfo*>* Logger::processLogs() {
	//have to find another way instead of using huuuuge vectors or arrays!(divide in blocks) TODO

	//Create LogMatrix
	auto logMatrix = createLogMatrix(*logs_, maxRefFrameN_ + 1, maxTestFrameN_ + 1); // BUG: matrix to big if unequal streams
	//group diagonals
	std::vector<GroupedLoggingInfo*> logGroups;
	for (unsigned int i = 0; i < logMatrix->size(); i++) {
		for (unsigned int j = 0; j < logMatrix->at(0).size(); j++) {
			auto group = getDiagonalGroupAt(*logMatrix, i, j);
			if (group != nullptr) //catch exception
				logGroups.push_back(group);
		}
	}
	std::vector<GroupedLoggingInfo*>* endVector = new std::vector<GroupedLoggingInfo*>();
	//find greatest matchgroup
	//for every enum
	//while count != 0
	//for every group
	//check if biggest
	//endfor
	//store biggest in ENDVECTOR
	//for every group
	//if (biggest overlaps with group)
	//remove group or specific overlapping log
	//endif
	//endfor
	//end while
	//endfor

	GroupedLoggingInfo* biggestGroup;
	//foreach log value
	for (auto matchInt = (int)Log::match; matchInt <= (int)Log::nomatch; matchInt++) {
		unsigned int count = -1;

		while (count != 0) {
			count = 0;
			biggestGroup = nullptr;
			for (GroupedLoggingInfo* group : logGroups) {
				if ((group->log == static_cast<Log>(matchInt)) && ((biggestGroup == nullptr) || (group->logCount > biggestGroup->logCount))) {
					if (biggestGroup == nullptr) {
						biggestGroup = group;
					} else {
						biggestGroup = group;
						count++;
					}
				} else if ((group->log == static_cast<Log>(matchInt))) {
					count++;
				}
			}
			if (biggestGroup != nullptr) {
				endVector->push_back(biggestGroup);

				for (unsigned int i = 0; i < logGroups.size(); i++) {
					auto group = logGroups.at(i);
					//if subsection of biggestGroup -> entire group gets removed
					if (biggestGroup->refFrameNEnd == group->refFrameNEnd && biggestGroup->testFrameNEnd == group->testFrameNEnd) {
						logGroups.erase(logGroups.begin() + i);
						i--;
					} else if (logGroupsOverlap(*biggestGroup, *group)) {
						auto newGroups = removeOverlap(*biggestGroup, *group);
						//add newGroups to logGroups
						logGroups.erase(logGroups.begin() + i);
						i--;
						logGroups.insert(std::end(logGroups), std::begin(*newGroups), std::end(*newGroups));
					}
				}
			}
		}
	}
	return endVector;
}

GroupedLoggingInfo* Logger::getDiagonalGroupAt(const std::vector<std::vector<LoggingInfo*>> &logMatrix, const unsigned int x, const unsigned int y) {
	std::vector<LoggingInfo*> logs;
	LoggingInfo* info = logMatrix.at(x).at(y);
	if (info == nullptr)
		return nullptr;
	logs.push_back(info);
	Log log = info->log;

	unsigned int i = x + 1;
	unsigned int j = y + 1;
	while (i < logMatrix.size() && j < logMatrix.at(0).size()) {
		if (logMatrix.at(i).at(j) == nullptr) {
			logs.push_back(new LoggingInfo(i, j, log));
			++i;
			++j;
		} else if (logMatrix.at(i).at(j)->log == log) {
			logs.push_back(logMatrix.at(i).at(j));
			++i;
			++j;
		} else {
			break;
		}
	}
	GroupedLoggingInfo* group = new GroupedLoggingInfo(x, i - 1, y, j - 1);
	group->logs = std::make_unique<std::vector<LoggingInfo*>>(logs);
	group->log = log;
	return group;
}

std::unique_ptr<std::vector<std::vector<LoggingInfo*>>> Logger::createLogMatrix(std::vector<LoggingInfo>& logs, const unsigned int width, const unsigned int height) {
	std::vector<std::vector<LoggingInfo*>> logMatrix(width, std::vector<LoggingInfo*>(height)); //size should be max framenumber
	for (auto &log : logs) {
		//put log in logmatrix
		//if log is already in position, combine the logs with the last one prioritized
		if (logMatrix[log.refFrameN][log.testFrameN] != nullptr) {
			//combine logs
			*(logMatrix[log.refFrameN][log.testFrameN]) << log; //overload << operator
		} else {
			logMatrix[log.refFrameN][log.testFrameN] = &log;
		}
	}
	return std::make_unique<std::vector<std::vector<LoggingInfo*>>>(logMatrix);
}

bool Logger::logGroupsOverlap(GroupedLoggingInfo& g1, GroupedLoggingInfo& g2) {
	if (g1.refFrameNBegin > g2.refFrameNBegin && g1.refFrameNBegin < (g2.refFrameNBegin + g2.logCount)) {
		return true;
	} else if (g1.refFrameNEnd < g2.refFrameNEnd && g1.refFrameNEnd >(g2.refFrameNEnd - g2.logCount)) {
		return true;
	} else if (g1.refFrameNBegin <= g2.refFrameNBegin && g1.refFrameNEnd >= g2.refFrameNEnd) {
		return true;
	}

	if (g1.testFrameNBegin > g2.testFrameNBegin && g1.testFrameNBegin < (g2.testFrameNBegin + g2.logCount)) {
		return true;
	} else if (g1.testFrameNEnd < g2.testFrameNEnd && g1.testFrameNEnd >(g2.testFrameNEnd - g2.logCount)) {
		return true;
	} else if (g1.testFrameNBegin <= g2.testFrameNBegin && g1.testFrameNEnd >= g2.testFrameNEnd) {
		return true;
	}
	return false;
}

std::unique_ptr<std::vector<GroupedLoggingInfo*>> Logger::removeOverlap(const GroupedLoggingInfo& org, GroupedLoggingInfo& lay) {
	std::vector<GroupedLoggingInfo*> newGroups;
	for (unsigned int i = 0; i < lay.logs->size();) { //= is bug waiting to happen!! make while loop!!
		auto info = lay.logs->at(i);
		//if info.ref is between org.ref B&E
		// ||
		//if info.test is between org.test B&E
		if ((info->refFrameN >= org.refFrameNBegin && info->refFrameN <= org.refFrameNEnd) || (info->testFrameN >= org.testFrameNBegin && info->testFrameN <= org.testFrameNEnd)) {
			//handle if first log
			if (i > 0) {
				GroupedLoggingInfo* group = new GroupedLoggingInfo(lay.refFrameNBegin, lay.logs->at(i - 1)->refFrameN, lay.testFrameNBegin, lay.logs->at(i - 1)->testFrameN);
				//put previous logs excluding current log in new group
				//auto it = std::end(*group->logs);
				group->logs = std::make_unique<std::vector<LoggingInfo*>>(std::begin(*(lay.logs)), std::begin(*(lay.logs)) + (i - 1));
				//group->logs->insert(std::end(*(group->logs)), std::begin(*(lay.logs)), std::begin(*(lay.logs)) + (i - 1));
				newGroups.push_back(group);
			}

			//remove all previous including current log
			lay.logs->erase(std::begin(*(lay.logs)), std::begin(*(lay.logs)) + i + 1);
			i = 0;
			//update lay
			if (i < lay.logs->size()) { // +1???
				lay.refFrameNBegin = lay.logs->at(i)->refFrameN;
				lay.testFrameNBegin = lay.logs->at(i)->testFrameN;
				lay.logCount = lay.refFrameNEnd - lay.refFrameNBegin; // CAN CAUSE BUGS??
			}
		} else {
			i++;
		}
	}
	//add remaining loggroup in newGroups TODO if remaining lay exists!!
	if (lay.logs->size() > 0) {
		newGroups.push_back(&lay);
	}
	return std::make_unique<std::vector<GroupedLoggingInfo*>>(newGroups);
}
} // namespace utils
} // namespace eurofins