#pragma once
#include <memory>
#include <vector>

namespace eurofins {
namespace utils {
/*
Put Logs in order of relevance
*/
enum class Log
{
	match,
	resized,
	colorchanged,
	black,
	freeze,
	nomatch
};

struct LoggingInfo {
	LoggingInfo(unsigned int refFrameN, unsigned int testFrameN, Log log) : refFrameN(refFrameN), testFrameN(testFrameN), log(log) {}
	unsigned int refFrameN;
	unsigned int testFrameN;
	Log log;
	float similarity;

	friend LoggingInfo & operator<<(LoggingInfo& log1, const LoggingInfo& log2)
	{
		log1.refFrameN = log2.refFrameN;
		log1.testFrameN = log2.testFrameN;
		log1.log = log2.log;
		log1.similarity = log2.similarity;
		return log1; //TODO
	}
};

struct GroupedLoggingInfo {
	GroupedLoggingInfo(unsigned int refFrameNBegin, unsigned int refFrameNEnd, unsigned int testFrameNBegin, unsigned int testFrameNEnd) : refFrameNBegin(refFrameNBegin), refFrameNEnd(refFrameNEnd), testFrameNBegin(testFrameNBegin), testFrameNEnd(testFrameNEnd), logCount(refFrameNEnd - refFrameNBegin) {}
	unsigned int refFrameNBegin;
	unsigned int refFrameNEnd;
	unsigned int testFrameNBegin;
	unsigned int testFrameNEnd;
	unsigned int logCount;
	std::unique_ptr<std::vector<LoggingInfo*>> logs;
	Log log;
};
class Logger
{
public:
	Logger();
	~Logger();
	void log(LoggingInfo&);
	void outputAsXML();
	void outputCLI();
private:
	std::unique_ptr<std::vector<LoggingInfo>> logs_;
	unsigned int maxRefFrameN_ = 0;
	unsigned int maxTestFrameN_ = 0;

	std::vector<GroupedLoggingInfo*>* processLogs();
	GroupedLoggingInfo* getDiagonalGroupAt(const std::vector<std::vector<LoggingInfo*>>& logMatrix, const unsigned int x, const unsigned int y);
	std::unique_ptr<std::vector<std::vector<LoggingInfo*>>> createLogMatrix(std::vector<LoggingInfo>& logs, const unsigned int width, const unsigned int height);
	bool logGroupsOverlap(GroupedLoggingInfo& g1, GroupedLoggingInfo& g2);
	std::unique_ptr<std::vector<GroupedLoggingInfo*>> removeOverlap(const GroupedLoggingInfo& org, GroupedLoggingInfo& lay);
};
} // namespace utils
} // namespace eurofins