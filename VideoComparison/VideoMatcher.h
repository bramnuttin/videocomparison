#pragma once
#include <memory>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>

namespace eurofins {
namespace video {
enum VideoState {
	matvector,
	intvector,
	videocapture
};
struct video {
	int state_id;
	union {
		std::shared_ptr<std::vector<cv::Mat>> s1;
		std::shared_ptr<std::vector<int>> s2;
		std::shared_ptr<cv::VideoCapture> s3;
	};
};
class VideoMatcher
{
public:
	VideoMatcher();
	~VideoMatcher();

private:
};
} // namespace video
} // namespace eurofins
