#include "TL_ImHash.h"
#include <opencv2/core.hpp>

TL_ImHash::TL_ImHash(cv::Mat & im, int nrWinInW /*= 1*/, int nrWinInH /*= 1*/)
	: m_nestedImage(im), m_nrOfWidthWindows(nrWinInW), m_nrOfHeightWindows(nrWinInH)
{
	int nrOfWin = nrOfWindows();

	m_nrOfWidthWindows = nrWinInW;
	m_nrOfHeightWindows = nrWinInH;
	m_nestedImage.subDivide(m_nrOfWidthWindows, m_nrOfHeightWindows);

	m_hash = new float[nrOfWin];

	for (int i = 0; i < nrOfWin; ++i)
		m_hash[i] = 0.0f;
}

TL_ImHash::~TL_ImHash()
{
	delete[] m_hash;
}

unsigned int TL_ImHash::nrOfWindows() const
{
	return m_nrOfWidthWindows * m_nrOfHeightWindows;
}