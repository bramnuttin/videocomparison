#include <opencv2/core.hpp>
#include "TL_NestedImage.h"
#include <xiosbase>
// interface class

#ifndef TL_IMAGEHASHBUILDER_H
#define TL_IMAGEHASHBUILDER_H

struct ADE_HashType { // average, deviation, edge hash type
	enum HASH_Test { AVG, STDEV, LEFT, RIGHT, TOP, BOTTOM, HASH_SIZE };

	float subHash[HASH_SIZE];
};

struct TL_Hash_Matching
{
	ADE_HashType m_perc;

	int m_nrOfSatisfyingWin; // allow certain features
	int m_totalNrOfWin;
	bool m_t[ADE_HashType::HASH_SIZE];

	TL_Hash_Matching(int nrWinToMatch, float avg = 100, float stdev = 100, float l = 100, float r = 100, float t = 100, float b = 100)
	{
		m_perc.subHash[ADE_HashType::AVG] = avg;
		m_perc.subHash[ADE_HashType::STDEV] = stdev;
		m_perc.subHash[ADE_HashType::LEFT] = l;
		m_perc.subHash[ADE_HashType::RIGHT] = r;
		m_perc.subHash[ADE_HashType::TOP] = t;
		m_perc.subHash[ADE_HashType::BOTTOM] = b;
		m_nrOfSatisfyingWin = nrWinToMatch;

		for (int i = 0; i < ADE_HashType::HASH_SIZE; ++i)
			m_t[i] = true;
	}

	unsigned int nrOfActiveOptions() const
	{
		unsigned int t = 0;

		for (int j = 0; j < ADE_HashType::HASH_SIZE; ++j)
			if (m_t[j])
				++t;

		return t;
	}

	unsigned int nrOfMatchingOptions() const
	{
		return ADE_HashType::HASH_SIZE;
	}

	void set(ADE_HashType::HASH_Test test, bool b)
	{
		m_t[test] = b;
	}
};

struct RGB_ADE_HashType {
	ADE_HashType color[3];
};

class TL_ImageHash
{
private:
	RGB_ADE_HashType* m_hash;
	size_t m_nrOfWidthWindows; // nr of windows in width direction
	size_t m_nrOfHeightWindows; //

	TL_NestedImage m_nestedImage;

	void computeHash(const cv::Mat & im);
	void computeAVG(const cv::Mat & im, const ROI & win, cv::Vec3f & rgbAVG);
	void computeSTDEV(const cv::Mat & im, const ROI & win, const cv::Vec3f & rgbAVG, cv::Vec3f & rgbDev);

	void computeHor(const cv::Mat & im, const ROI & win, cv::Vec3f & avgHorToRight, cv::Vec3f & avgHorToLeft);
	void computeVert(const cv::Mat & im, const ROI & win, cv::Vec3f & avgToBot, cv::Vec3f & avgToTop);

	float percOfSimularity(float v0, float v1);

public:
	// level of detail : how many windows in width and height direction
	TL_ImageHash(cv::Mat & im, size_t hNrOfWin = 0, size_t wNrOfWin = 0);
	~TL_ImageHash();

	unsigned int nrOfWindows() { return m_nrOfWidthWindows * m_nrOfHeightWindows; }
	float getTotalHash(/*TL_ImageHash & h*/);
	bool compare(TL_ImageHash & h1, TL_Hash_Matching & test, std::vector<float> & results);

	void showWindows(cv::Mat & im);
	void showHash();
	void saveHash(const char* f, std::ios_base::openmode m = std::ios::out | std::ios::binary);
	void loadHash(const char* f, std::ios_base::openmode m = std::ios::in | std::ios::binary);
	void logHash(const char* f);

	void saveToCSV(const char* f);
	void saveContentToCSV(std::ofstream & fs, const char* sep = ",");
	void saveHeaderToCSV(std::ofstream & fs, const char* sep = ",");
	void saveResultsToCSV(std::ofstream & of, int frameNr, std::vector<float> & results, const char* sep = ",");
};

#endif