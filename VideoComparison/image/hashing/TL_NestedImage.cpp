#include "TL_NestedImage.h"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
TL_NestedImage::TL_NestedImage(const cv::Mat& im) : m_parent(0)
{
	m_children.clear();

	m_location.startR = 0;
	m_location.startC = 0;

	m_location.endR = im.rows - 1;
	m_location.endC = im.cols - 1;
}

TL_NestedImage::~TL_NestedImage()
{
	//std::cout << "destructed nested image " << std::endl;

	//std::cout << "Children size = " << m_children.size() << std::endl;
	for (std::vector<TL_NestedImage*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
	{
		delete *it;
		*it = 0;
	}

	m_parent = 0;
	m_children.clear();
}

TL_NestedImage::TL_NestedImage(TL_NestedImage* image, unsigned int rPos, unsigned int colPos, unsigned int  h, unsigned int w)
	: m_parent(image)
{
	std::cout << "constructed nested image " << std::endl;

	const ROI & pROI = m_parent->getROI();

	m_location = ROI(pROI.startR + rPos, pROI.startC + colPos,
		pROI.startR + rPos + h, pROI.startC + colPos + w);

	std::cout << "\nSub image start (" << m_location.startR << "," << m_location.startC << ")"
		<< " end (" << m_location.endR << "," << m_location.endC << ")";

	m_children.clear();
}

void TL_NestedImage::setParent(const cv::Mat & p)
{
	delete m_parent;

	m_parent = 0;
}

void TL_NestedImage::subDivide(int nrOfWinInWidth, int nrOfWinInHeight)
{
	unsigned int width = m_location.width();
	unsigned int height = m_location.height();

	unsigned int heightRest = 0;
	unsigned int widthRest = 0;

	heightRest = height % nrOfWinInHeight; // allocate variable last window
	widthRest = width % nrOfWinInWidth;

	if (height < nrOfWinInHeight)
		throw "Nr of windows overflow in height of image";

	if (width < nrOfWinInWidth)
		throw "Nr of windows overflow in width of image";

	int winYSize = height / nrOfWinInHeight;
	int winXSize = width / nrOfWinInWidth;

	unsigned int xRest = winXSize;
	unsigned int yRest = winYSize;

	for (int i = 0; i < height;) {
		for (int j = 0; j < width;) {
			xRest = winXSize;
			yRest = winYSize;

			if (heightRest > 0 && (heightRest + winYSize + i) >= height) // make last window bigger
				yRest += heightRest;

			if (widthRest > 0 && (widthRest + winXSize + j) >= width)
				xRest += widthRest;

			m_children.push_back(new TL_NestedImage(this, i, j, yRest - 1, xRest - 1));

			j += xRest;
		}

		i += yRest;
	}

	// 	std::cout << "Children size = " << m_children.size() << std::endl;
	// 	std::cout << "nr of windows = " << nrOfWinInWidth * nrOfWinInHeight << std::endl;
	assert(m_children.size() == nrOfWinInWidth * nrOfWinInHeight);
}

void TL_NestedImage::drawDivisions(cv::Mat & root)
{
	cv::Size s;
	cv::Point begin;

	// 	std::cout << "\nDraw image start (" << m_location.startR << "," << m_location.startC << ")"
	// 		<< " end (" << m_location.endR << "," << m_location.endC << ")";

	cv::rectangle(root, cv::Point(m_location.startC, m_location.startR),
		cv::Point(m_location.endC, m_location.endR),
		cv::Scalar(255, 0, 0), 2.0, 8.0);

	for (int i = 0; i < m_children.size(); ++i) {
		if (m_children[i])
			m_children[i]->drawDivisions(root);
	}
}

const ROI & TL_NestedImage::operator[](int i)
{
	if (i >= m_children.size())
		throw "Accessing non existing child";

	return m_children[i]->getROI();
}