#ifndef IMHASH_H
#define IMHASH_H
#include "TL_NestedImage.h"

class TL_ImHash
{
protected:
	float* m_hash;
	int m_nrOfWidthWindows; // nr of windows in width direction
	int m_nrOfHeightWindows; //
	TL_NestedImage m_nestedImage;

	unsigned int nrOfWindows() const;

public:

	TL_ImHash(cv::Mat & im, int nrWinInW = 1, int nrWinInH = 1);

	virtual ~TL_ImHash();
};

#endif