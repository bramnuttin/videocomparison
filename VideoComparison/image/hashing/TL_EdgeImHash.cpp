#include "TL_EdgeImHash.h"
#include "opencv2/core.hpp"
#include <iostream>
#include <fstream>

TL_EdgeImHash::TL_EdgeImHash(cv::Mat & im, int nrWinInW /*= 1*/, int nrWinInH /*= 1*/)
	: TL_ImHash(im, nrWinInW, nrWinInH)
{
	m_hash = cv::Vec2f(0, 0);
}

void TL_EdgeImHash::compute(cv::Mat & im)
{
	unsigned int nrOfSubIm = m_nestedImage.getNrOfSubImages();
	std::cout << "Nr of subimages = " << nrOfSubIm << std::endl;

	cv::Vec2f hashCenter, hashCorner, hybridHash;

	for (int i = 0; i < nrOfSubIm; ++i)
	{
		const ROI & subIm = m_nestedImage[i];

		//computeSpatialHashToCentre(im, subIm, hashCenter);
		//computeSpatialHashToCorners(im, subIm, m_hash);
		computeAVGHash(im, subIm, m_hash);
	}
}

void TL_EdgeImHash::compute(cv::Mat & im, Method m)
{
	unsigned int nrOfSubIm = m_nestedImage.getNrOfSubImages();
	std::cout << "Nr of subimages = " << nrOfSubIm << std::endl;

	cv::Vec2f hashCenter, hashCorner, hybridHash;

	m_hash = cv::Vec2f(0, 0);

	for (int i = 0; i < nrOfSubIm; ++i)
	{
		const ROI & subIm = m_nestedImage[i];

		//computeSpatialHashToCentre(im, subIm, hashCenter);

		switch (m)
		{
		case TL_EdgeImHash::CENTERZONE:
			computeSpatialHashToCorners(im, subIm, m_hash);
			break;
		case TL_EdgeImHash::CORNERZONE:
			computeSpatialHashToCentre(im, subIm, m_hash);
			break;
		}
		//computeAVGHash(im, subIm, hybridHash);
	}
}

int TL_EdgeImHash::countEdgePixels(const cv::Mat & edgeIm)
{
	int t = 0;

	for (int i = 0; i < edgeIm.rows; ++i)
		for (int j = 0; j < edgeIm.cols; ++j)
			if (edgeIm.at<uchar>(i, j) > 0)
				++t;

	return t;
}

void TL_EdgeImHash::computeSpatialHashToCentre(const cv::Mat & im, const ROI & win, cv::Vec2f & hash)
{
	unsigned int nrOfPixels = win.nrOfPixels();
	float sum = 0;
	float imCenterW = im.cols / 2.0f;
	float imCenterH = im.rows / 2.0f;

	int imCenterPixelX = imCenterW - 1;
	int imCenterPixelY = imCenterH - 1;

	std::cout << "Image center w = " << imCenterW << "\n";
	std::cout << "Image center h = " << imCenterH << "\n";

	//std::ofstream of("C:/TL/log/hash_centers.log");
	int edgePixels = countEdgePixels(im);

	//float edgeWeight = 1.0f / (float) countEdgePixels(im);

	for (int i = win.startR; i <= win.endR; ++i)
		for (int j = win.startC; j <= win.endC; ++j) {
			float edge = (float)im.at<uchar>(i, j);
			float wi = edge / 255.0f; // intensitiy weight

			if (wi > 0)
			{
				//wi *= edgeWeight;

				//wi = 1;
				cv::Vec2f temp(0, 0);

				getAsymHashFromCenter(i, j, imCenterPixelY, imCenterPixelX, temp);

				hash[0] += wi * temp[0];
				hash[1] += wi * temp[1];

				//of << "Pixel " << i << "," << j << " dist = (" << temp[0] << "," << temp[1] << ")\n";
			}
		}

	hash[0] /= nrOfPixels;
	hash[1] /= nrOfPixels;

	//of << "Spatial hash center = " << hash[0] << "," << hash[1] << "\n";

	//of.close();
}

void TL_EdgeImHash::computeAVGHash(const cv::Mat & im, const ROI & win, cv::Vec2f & hash)
{
	unsigned int nrOfPixels = win.nrOfPixels();

	int imCenterW = im.cols / 2;
	int imCenterH = im.rows / 2;

	int imCenterPixelX = imCenterW - 1;
	int imCenterPixelY = imCenterH - 1;

	std::ofstream of("./edge_hybrid.log");

	//float edgeWeight = 1.0f / (float) countEdgePixels(im);

	for (int i = win.startR; i <= win.endR; ++i)
		for (int j = win.startC; j <= win.endC; ++j) {
			float edge = (float)im.at<uchar>(i, j);
			float wi = edge / 255.0f;

			if (wi > 0)
			{
				//wi *= edgeWeight;

				cv::Vec2f temp(0, 0);
				cv::Vec2f temp1(0, 0);

				getASymHashFromCorners(im.rows, im.cols, i, j, imCenterPixelY, imCenterPixelX, temp);
				getAsymHashFromCenter(i, j, imCenterPixelY, imCenterPixelX, temp1);

				hash[0] += wi * (temp[0] + temp1[0]);
				hash[1] += wi * (temp[1] + temp1[1]);

				//wi = 1;
				of << "pixel " << i << "," << j << "\n";
				of << "Pixel " << i << "," << j << " dist = (" << temp[0] << "," << temp[1] << ")\n";
			}
		}

	hash[0] /= nrOfPixels;
	hash[1] /= nrOfPixels;

	of << "Spatial hash corners = " << hash[0] << "," << hash[1] << "\n";

	of.close();
}

void TL_EdgeImHash::getSymHashFromCenter(int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out)
{
	out[0] = i - imCenterPixelX; // first pixel has distance imCenterH
	out[1] = j - imCenterPixelY;
}

void TL_EdgeImHash::getAsymHashFromCenter(int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out)
{
	getSymHashFromCenter(i, j, imCenterPixelY, imCenterPixelX, out);

	out[0] = std::fabs(out[0]);
	out[1] = std::fabs(out[1]);
}

void TL_EdgeImHash::getASymHashFromCorners(int imH, int imW, int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out)
{
	getSymHashFromCorners(imH, imW, i, j, imCenterPixelY, imCenterPixelX, out);

	out[0] = std::fabs(out[0]);
	out[1] = std::fabs(out[1]);
}

void TL_EdgeImHash::getSymHashFromCorners(int imH, int imW, int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out)
{
	int lastHPixel = imH - 1; // rows
	int lastWPixel = imW - 1; // cols

	if (i <= imCenterPixelY && j <= imCenterPixelX) // first quadrant, pos
	{
		out[0] = (i + 1);
		out[1] = (j + 1);

		//		of << "First quadrant " << out[0] << "," << out[1] << "\n";
	}

	if (i <= imCenterPixelY && j >= imCenterPixelX) // second quadrant, neg
	{
		out[0] = -(i + 1);
		out[1] = j - lastWPixel;

		//		of << "Second quadrant " << out[0] << "," << out[1] << "\n";
	}

	if (i >= imCenterPixelY && j <= imCenterPixelX) // third quadrant, pos
	{
		out[0] = lastHPixel - i;
		out[1] = j + 1;

		//		of << "Third quadrant " << out[0] << "," << out[1] << "\n";
	}

	if (i >= imCenterPixelY && j >= imCenterPixelX) // fourth quadrant, neg
	{
		out[0] = i - lastHPixel;
		out[1] = j - lastWPixel;

		//		of << "Fourth quadrant " << out[0] << "," << out[1] << "\n";
	}
}

void TL_EdgeImHash::computeSpatialHashToCorners(const cv::Mat & im, const ROI & win, cv::Vec2f & hash)
{
	unsigned int nrOfPixels = win.nrOfPixels();

	int imCenterW = im.cols / 2;
	int imCenterH = im.rows / 2;

	int imCenterPixelX = imCenterW - 1;
	int imCenterPixelY = imCenterH - 1;

	//float edgeWeight = 1.0f / (float) countEdgePixels(im);

	std::ofstream of("./hash_corners.log");

	for (int i = win.startR; i <= win.endR; ++i)
		for (int j = win.startC; j <= win.endC; ++j) {
			float edge = (float)im.at<uchar>(i, j);
			float wi = edge / 255.0f;

			if (wi > 0)
			{
				//wi = 1;
				//wi *= edgeWeight;
				cv::Vec2f temp(0, 0);

				getASymHashFromCorners(im.rows, im.cols, i, j, imCenterPixelY, imCenterPixelX, temp);

				hash[0] += wi * temp[0];
				hash[1] += wi * temp[1];

				//wi = 1;
				of << "pixel " << i << "," << j << "\n";
				of << "Pixel " << i << "," << j << " dist = (" << temp[0] << "," << temp[1] << ")\n";
			}
		}

	hash[0] /= nrOfPixels;
	hash[1] /= nrOfPixels;

	of << "Spatial hash corners = " << hash[0] << "," << hash[1] << "\n";

	of.close();
}