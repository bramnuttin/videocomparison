#include "TL_ImHash.h"
#include "TL_NestedImage.h"

#ifndef TL_EDGEIMHASH_H
#define TL_EDGEIMHASH_H

class TL_EdgeImHash : public TL_ImHash
{
	cv::Vec2f m_hash;

public:
	enum Method { CENTERZONE, CORNERZONE };

	TL_EdgeImHash(cv::Mat & im, int nrWinInW = 1, int nrWinInH = 1);

	const cv::Vec2f & getHash() const { return m_hash; }
	void compute(cv::Mat & im);
	void compute(cv::Mat & im, Method m);
	void computeSpatialHashToCorners(const cv::Mat & im, const ROI & win, cv::Vec2f & hash);
	void computeAVGHash(const cv::Mat & im, const ROI & win, cv::Vec2f & hash);
	void computeSpatialHashToCentre(const cv::Mat & im, const ROI & win, cv::Vec2f & hash);
	void getSymHashFromCorners(int imH, int imW, int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out);
	void getAsymHashFromCenter(int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out);
	void getSymHashFromCenter(int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out);
	void getASymHashFromCorners(int imH, int imW, int i, int j, int imCenterPixelY, int imCenterPixelX, cv::Vec2f & out);
	int countEdgePixels(const cv::Mat & edgeIm);
};

#endif