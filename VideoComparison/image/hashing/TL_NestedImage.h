#ifndef TL_NESTEDIMAGE
#define TL_NESTEDIMAGE
#include <vector>
#include <opencv2/core.hpp>

struct ROI	   // region of interest
{
	unsigned int startR; // start row
	unsigned int startC; // start column

	unsigned int endR; // end row
	unsigned int endC; // end column

	ROI()
	{
		startR = startC = endC = endR = 0;
	}

	ROI(unsigned int sR, unsigned int sC, unsigned int eR, unsigned int eC)
		: startR(sR), startC(sC), endR(eR), endC(eC)
	{
	}

	bool contains(unsigned int pR, unsigned int pC)
	{
		return pR >= startR && pC >= startC && pR <= endR && pC <= endC;
	}

	bool containsRow(unsigned int pR)
	{
		return pR >= startR && pR >= endR;
	}

	/*

	std::list<ROI> operator+(const ROI & r)
	{
		std::list<ROI> l;

		l.push_back(r);
		l.push_back(*this);

		// first test if it is not fully contained

		// corner overlap
		if(contains(r.startR, r.startC))
			l.push_back(ROI(r.startR, r.startC, endR, endC));

		if(contains(r.startR, r.endC))
			l.push_back(ROI(r.startR, startC, endR, r.endC));

		if(contains(r.endR, r.endC))
			l.push_back(ROI(startR, startC, r.endR, r.endC));

		if(contains(r.endR, r.startC))
			l.push_back(ROI(startR, r.startC, r.endR, endC));

		if (containsRow(startR))
			l.push_back();
	}

	*/

	unsigned int width() const { return (endC - startC) + 1; } // endC = lastC + 1
	unsigned int height() const { return (endR - startR) + 1; } // endR = lastR + 1
	unsigned int nrOfPixels() const { return width() * height(); }
};

class TL_NestedImage
{
private:
	ROI m_location;

	TL_NestedImage* m_parent;
	std::vector<TL_NestedImage*> m_children;

	TL_NestedImage(TL_NestedImage* parent, unsigned int rPos, unsigned int colPos, unsigned int h, unsigned int w);

public:

	TL_NestedImage(const cv::Mat & mat);
	~TL_NestedImage();

	//
	// 	void writeRect( Mat & frame, const Mat & rect, Scalar color )
	// 	{
	// 		if (rect.rows == 4 && rect.cols == 1)
	// 		{
	// 			double width = rect.at<double>(cv::Point2d(0,2));
	// 			double height = rect.at<double>(cv::Point2d(0,3));
	// 			float thick = 2.0;
	// 			float type = 8.0;
	//
	// 			cv::Point2d begin = cv::Point2d(rect.at<double>(Point2d(0,0)), rect.at<double>(Point2d(0,1)));
	// 			cv::Point2d end = cv::Point2d(begin.x + width, begin.y + height);
	// 			cv::rectangle(frame, begin, end, color, thick, type);
	// 		}
	// 	}

	const ROI & getROI() const { return m_location; }
	TL_NestedImage* getParent() { return m_parent; }
	void setParent(const cv::Mat & p);

	void drawDivisions(cv::Mat & root);
	void subDivide(int nrOfWinInWidth, int nrOfWinInHeight);

	const ROI & operator[](int i);

	unsigned int getNrOfSubImages() const { return m_children.size(); }
	TL_NestedImage* getSubImage(int i) { if (i < m_children.size()) return m_children[i]; else return 0; }

	bool isParent()
	{
		return m_parent == 0;
	}

	bool isLeaf()
	{
		return m_children.size() == 0;
	}
};

#endif