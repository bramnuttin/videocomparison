#include "TL_ImageHash.h"
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;
TL_ImageHash::TL_ImageHash(cv::Mat & im, size_t nrOfWinH, size_t nrOfWinW)
	: m_nestedImage(im)
{
	// return on non divideable image,

	m_nrOfWidthWindows = nrOfWinW;
	m_nrOfHeightWindows = nrOfWinH;

	m_hash = new RGB_ADE_HashType[m_nrOfWidthWindows * m_nrOfHeightWindows];

	m_nestedImage.subDivide(m_nrOfHeightWindows, m_nrOfWidthWindows);

	computeHash(im);
}

TL_ImageHash::~TL_ImageHash()
{
	delete[] m_hash;
}

void TL_ImageHash::computeHash(const cv::Mat & im)
{
	cv::Vec3f subH[ADE_HashType::HASH_SIZE]; // color avg // detail avg // edge avg, with quadratic weight for big differences
	unsigned int nrOfSubIm = m_nestedImage.getNrOfSubImages();
	std::cout << "Nr of subimages = " << nrOfSubIm << std::endl;

	for (int i = 0; i < nrOfSubIm; ++i)
	{
		const ROI & subIm = m_nestedImage[i];

		// 		std::cout << "Sub image start (" << subIm.startR << "," << subIm.startC << ")"
		// 			<< "\n end(" << subIm.endR << "," << subIm.endC << ")";

		computeAVG(im, subIm, subH[ADE_HashType::AVG]);
		computeSTDEV(im, subIm, subH[ADE_HashType::AVG], subH[ADE_HashType::STDEV]);
		computeHor(im, subIm, subH[ADE_HashType::RIGHT], subH[ADE_HashType::LEFT]);
		computeVert(im, subIm, subH[ADE_HashType::BOTTOM], subH[ADE_HashType::TOP]);

		for (int c = 0; c < im.channels(); ++c)
		{
			// copy calculations
			for (int h = 0; h < ADE_HashType::HASH_SIZE; ++h) {
				m_hash[i].color[c].subHash[h] = subH[h][2 - c]; // opencv's default is bgr
			}
		}
	}
}

// multithreaded usage: winage is copy of pixels in winage window
void TL_ImageHash::computeAVG(const cv::Mat & im, const ROI & win, cv::Vec3f & rgbAVG)
{
	rgbAVG = cv::Vec3f(0, 0, 0);

	unsigned int nrOfPixels = win.nrOfPixels();

	for (unsigned int i = win.startR; i <= win.endR; ++i)
		for (unsigned int j = win.startC; j <= win.endC; ++j) {
			rgbAVG[0] += im.at<cv::Vec3b>(i, j)[0];
			rgbAVG[1] += im.at<cv::Vec3b>(i, j)[1];
			rgbAVG[2] += im.at<cv::Vec3b>(i, j)[2];
		}

	rgbAVG[0] /= nrOfPixels;
	rgbAVG[1] /= nrOfPixels;
	rgbAVG[2] /= nrOfPixels;
}

// standard deviation: root of average variation to the mean
// compute deviation of winage win
void TL_ImageHash::computeSTDEV(const cv::Mat & im, const ROI & win, const cv::Vec3f & rgbAVG, cv::Vec3f & rgbDev)
{
	rgbDev = cv::Vec3f(0, 0, 0);

	unsigned int nrOfPixels = win.nrOfPixels();

	for (unsigned int i = win.startR; i <= win.endR; ++i)
		for (unsigned int j = win.startC; j <= win.endC; ++j) {
			rgbDev[0] += powf(im.at<cv::Vec3b>(i, j)[0] - rgbAVG[0], 2);
			rgbDev[1] += powf(im.at<cv::Vec3b>(i, j)[1] - rgbAVG[1], 2);
			rgbDev[2] += powf(im.at<cv::Vec3b>(i, j)[2] - rgbAVG[2], 2);
		}

	rgbDev[0] = sqrtf(rgbDev[0] / nrOfPixels);
	rgbDev[1] = sqrtf(rgbDev[1] / nrOfPixels);
	rgbDev[2] = sqrtf(rgbDev[2] / nrOfPixels);
}

void TL_ImageHash::computeHor(const cv::Mat & im, const ROI & win, cv::Vec3f & avgHorToRight, cv::Vec3f & avgHorToLeft)
{
	avgHorToRight = cv::Vec3f(0, 0, 0);
	avgHorToLeft = cv::Vec3f(0, 0, 0);

	for (int i = win.startR; i <= win.endR; ++i) {
		for (int j = win.startC; j <= win.endC - 1; ++j) {
			for (int c = 0; c < im.channels(); ++c) {
				int val = im.at<cv::Vec3b>(i, j + 1)[c] - im.at<cv::Vec3b>(i, j)[c];

				if (val >= 0)
					avgHorToRight[c] += val*val;
				else
					avgHorToLeft[c] += val*val;
			}
		}
	}

	for (int c = 0; c < im.channels(); ++c) {
		avgHorToRight[c] /= win.nrOfPixels();
		avgHorToLeft[c] /= win.nrOfPixels();
	}
}

void TL_ImageHash::computeVert(const cv::Mat & im, const ROI & win, cv::Vec3f & avgToBot, cv::Vec3f & avgToTop)
{
	avgToBot = cv::Vec3f(0, 0, 0);
	avgToTop = cv::Vec3f(0, 0, 0);

	for (int i = win.startC; i <= win.endC; ++i) {
		for (int j = win.startR; j <= win.endR - 1; ++j) {
			for (int c = 0; c < im.channels(); ++c) {
				int val = im.at<cv::Vec3b>(j + 1, i)[c] - im.at<cv::Vec3b>(j, i)[c];

				if (val >= 0)
					avgToBot[c] += val * val;
				else
					avgToTop[c] += val * val;
			}
		}
	}

	for (int c = 0; c < im.channels(); ++c) {
		avgToBot[c] /= win.nrOfPixels();
		avgToTop[c] /= win.nrOfPixels();
	}
}

void TL_ImageHash::saveHash(const char* f, ios_base::openmode m)
{
	std::ofstream fs(f, m);

	int nrOfWindows = m_nrOfHeightWindows * m_nrOfWidthWindows;

	if (!fs.good())
		throw "Non existing output directory";

	fs.write((char*)&nrOfWindows, sizeof(int));

	for (int i = 0; i < nrOfWindows; ++i)
		fs.write((char*)&m_hash[i], sizeof(RGB_ADE_HashType));

	fs.close();
}

void TL_ImageHash::logHash(const char* f)
{
	std::ofstream fs(f, ios_base::out);

	int nrOfWindows = m_nrOfHeightWindows * m_nrOfWidthWindows;

	if (!fs.good())
		throw "Non existing output directory";

	fs << "Nr of windows " << nrOfWindows << "\n";

	for (int i = 0; i < nrOfWindows; ++i) {
		fs << "win " << i << "\n";

		for (int j = 0; j < 3; ++j) {
			switch (j)
			{
			case 0:
				fs << "\nr\n";
				break;
			case 1:
				fs << "\ng\n";
				break;
			case 2:
				fs << "b\n";
				break;
			}

			fs << std::setw(10) << "avg " << m_hash[i].color[j].subHash[ADE_HashType::AVG] << std::endl;
			fs << std::setw(10) << "dev " << m_hash[i].color[j].subHash[ADE_HashType::STDEV] << std::endl;
			fs << std::setw(10) << "top " << m_hash[i].color[j].subHash[ADE_HashType::TOP] << std::endl;
			fs << std::setw(10) << "bot " << m_hash[i].color[j].subHash[ADE_HashType::BOTTOM] << std::endl;
			fs << std::setw(10) << "right " << m_hash[i].color[j].subHash[ADE_HashType::RIGHT] << std::endl;
			fs << std::setw(10) << "left " << m_hash[i].color[j].subHash[ADE_HashType::LEFT] << std::endl;
		}
	}
	fs.close();
}

void TL_ImageHash::saveHeaderToCSV(std::ofstream & fs, const char* sep)
{
	fs << std::setw(12) << "window" << sep
		<< std::setw(12) << "rgb" << sep
		<< std::setw(12) << "avg" << sep << std::setw(12) << "dev" << sep << std::setw(12) << "left" << sep
		<< std::setw(12) << "right" << sep << std::setw(12) << "top" << sep << std::setw(12) << "bottom" << "\n";
}

void TL_ImageHash::saveContentToCSV(std::ofstream & fs, const char* sep)
{
	int nrOfWindows = m_nrOfHeightWindows * m_nrOfWidthWindows;

	for (int i = 0; i < nrOfWindows; ++i) {
		for (int j = 0; j < 3; ++j) {
			fs << std::setw(12) << i << sep << std::setw(12) << j;

			for (int h = 0; h < ADE_HashType::HASH_SIZE; ++h) {
				fs << sep << std::setw(12) << m_hash[i].color[j].subHash[h];
			}
			fs << "\n";
		}
	}
}

void TL_ImageHash::saveResultsToCSV(std::ofstream & of, int frameNr, std::vector<float> & results, const char* sep)
{
	int nrOfWin = nrOfWindows();
	int nrOfColors = 3;
	int nrOfSubHashes = ADE_HashType::HASH_SIZE;

	assert(nrOfWin * nrOfColors * nrOfSubHashes == results.size());

	for (int i = 0; i < nrOfWin; ++i) {
		for (int c = 0; c < nrOfColors; ++c) {
			of << std::setw(12) << frameNr << sep << std::setw(12) << i << sep << std::setw(12) << c;

			for (int h = 0; h < nrOfSubHashes; ++h) {
				of << sep << std::setw(12) << std::setprecision(10) << results[i*nrOfColors*nrOfSubHashes + c * nrOfSubHashes + h];
			}

			of << "\n";
		}
	}
}

void TL_ImageHash::saveToCSV(const char* f)
{
	std::ofstream fs(f, ios_base::out);

	if (!fs.good())
		throw "Error: non existing output directory?";

	fs << "sep=,\n";
	saveHeaderToCSV(fs);
	saveContentToCSV(fs);

	fs.close();
}

void TL_ImageHash::loadHash(const char* f, ios_base::openmode m)
{
	std::ifstream fs(f, m);
	int nrOfWindows = m_nrOfHeightWindows * m_nrOfWidthWindows;

	fs.read((char*)&nrOfWindows, sizeof(int));

	m_hash = new RGB_ADE_HashType[nrOfWindows];

	for (int i = 0; i < nrOfWindows; ++i)
		fs.read((char*)&m_hash[i], sizeof(RGB_ADE_HashType));

	fs.close();
}

void TL_ImageHash::showHash()
{
	int nrOfWin = m_nrOfHeightWindows * m_nrOfWidthWindows;
	float diff[ADE_HashType::HASH_SIZE];

	for (int i = 0; i < nrOfWin; ++i) {
		for (int c = 0; c < 3; ++c) {
			for (int h = 0; h < ADE_HashType::HASH_SIZE; ++h) {
				diff[h] += fabs(m_hash[i].color[c].subHash[h]);
			}
		}
	}

	std::cout << "\nAVG diff = " << diff[ADE_HashType::AVG]
		<< "\nDEV diff = " << diff[ADE_HashType::STDEV]
		<< "\nleft diff = " << diff[ADE_HashType::LEFT]
		<< "\nright diff = " << diff[ADE_HashType::RIGHT]
		<< "\ntop diff = " << diff[ADE_HashType::TOP]
		<< "\nbot diff = " << diff[ADE_HashType::BOTTOM]
		<< std::endl;
}

float TL_ImageHash::getTotalHash(/* TL_ImageHash & ih */)
{
	int nrOfWin = nrOfWindows();
	// 	int nrOfWin1 = ih.m_nrOfHeightWindows * ih.m_nrOfWidthWindows;
	//
	// 	assert(nrOfWin0 == nrOfWin1);
	//
	// 	float diff[ADE_HashType::HASH_SIZE];
	//
	// 	for (int i = 0 ; i < nrOfWin0; ++i){
	// 		for (int c = 0; c < 3; ++c){
	// 			for (int h = 0; h < ADE_HashType::HASH_SIZE; ++h){
	// 				diff[h] += fabs(ih.m_hash[i].color[c].subHash[h]) - fabs(m_hash[i].color[c].subHash[h]);
	// 			}
	// 		}
	// 	}
	//
	// 	std::cout << "\nAVG diff = " << diff[ADE_HashType::AVG]
	// 			  << "\nDEV diff = " << diff[ADE_HashType::STDEV]
	// 			  << "\nleft diff = " << diff[ADE_HashType::LEFT]
	// 			  << "\nright diff = " << diff[ADE_HashType::RIGHT]
	// 			  << "\ntop diff = " << diff[ADE_HashType::TOP]
	// 			  << "\nbot diff = " << diff[ADE_HashType::BOTTOM]
	// 			  << std::endl;

	float sum = 0;

	for (int i = 0; i < nrOfWin; ++i) {
		for (int c = 0; c < 3; ++c) {
			for (int h = 0; h < ADE_HashType::HASH_SIZE; ++h) {
				sum += fabs(m_hash[i].color[c].subHash[h]);
			}
		}
	}

	return sum;
}

void TL_ImageHash::showWindows(cv::Mat & im)
{
	m_nestedImage.drawDivisions(im);
}

float TL_ImageHash::percOfSimularity(float v0, float v1)
{
	return std::min(v0, v1) / std::max(v0, v1);
}

bool TL_ImageHash::compare(TL_ImageHash & h1, TL_Hash_Matching & test, std::vector<float> & results)
{
	if (h1.nrOfWindows() != nrOfWindows())
		std::cout << "Unequal number of windows: comparing apples with pears!\n";

	if (test.m_nrOfSatisfyingWin > h1.nrOfWindows())
	{
		test.m_nrOfSatisfyingWin = h1.nrOfWindows();

		std::cout << "Satisfying window condition is higher than the nr of windows";
		std::cout << "Defaulting to max windows";
	}
	unsigned int nrOfWin = nrOfWindows();
	unsigned int nrOfSatisfactions = 0;

	unsigned int nrOfChannels = 3;
	unsigned int nrOfCritSat = 0;
	unsigned int nrOfCritToSatisfy = nrOfChannels * test.nrOfActiveOptions();

	for (int i = 0; i < nrOfWin; ++i)
	{
		nrOfCritSat = 0;

		// iterate colors
		for (int c = 0; c < 3; ++c) {
			for (int h = 0; h < ADE_HashType::HASH_SIZE; ++h) {
				if (test.m_t[h]) {
					float percOfSim = percOfSimularity(m_hash[i].color[c].subHash[h], h1.m_hash[i].color[c].subHash[h]);

					std::cout << "percOfSim" << percOfSim << std::endl;

					results.push_back(percOfSim);

					if (percOfSim >= test.m_perc.subHash[h])
					{
						++nrOfCritSat;
					}
				}
			}
		}

		if (nrOfCritSat == nrOfCritToSatisfy)
			++nrOfSatisfactions;
	}

	if (nrOfSatisfactions >= test.m_nrOfSatisfyingWin)
		return true;

	return false;
}