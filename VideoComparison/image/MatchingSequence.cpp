#include "MatchingSequence.h"

namespace eurofins {
namespace image {
using utils::TreeNode;
using utils::Logger;
using utils::Config;
MatchingSequence::MatchingSequence(TreeNode<IAlgorithm>* tree) : tree_(tree)
{
}

MatchingSequence::~MatchingSequence()
{
}

float MatchingSequence::executeSequence(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger, utils::Config& config) const
{
	float result = -1;
	TIMEBLOCK("Tree execution") {
		result = execute(tree_, refImg, testImg, refFrameN, testFrameN, logger, config);
	}
	return result;
}

float MatchingSequence::execute(const TreeNode<IAlgorithm>* tree, cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger, utils::Config& config) const
{
	if (tree->algo == NULL)
		return true;
	float score = tree->algo->execute(refImg, testImg, refFrameN, testFrameN, logger, config);
	if (tree->children->size() > 0) {
		for each (const TreeNode<IAlgorithm>* n in *(tree->children)) {
			if (n->threshold[0] <= score && n->threshold[1] >= score || score == -1) {
				return execute(n, refImg, testImg, refFrameN, testFrameN, logger, config);
			}
		}
	}
	return score;
}
} // namespace image
} // namespace eurofins