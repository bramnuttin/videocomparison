#include "ImageMatcher.h"
#include <iostream>
#include "../utils/logging/Logger.h"
#include "../utils/config/AlgoStringSequenceBuilder.h"
#include "SequenceFactory.h"
namespace eurofins {
namespace image {
using utils::Config;
using utils::Logger;
ImageMatcher::ImageMatcher(Config& cfg) {
	cfg_ = std::make_shared<Config>(cfg);
	//auto algostringTree = cfg_->getSequenceStringTreeFromKey(std::string("imagematching"));
	//matchingSequence_ = SequenceFactory::makeSequence(algostringTree);
	matchingSequence_ = std::make_shared<MatchingSequence>(cfg_->getSequenceTreeFromKey<IAlgorithm>(std::string("imagematching"), &(SequenceFactory::makeAlgorithm)));
	//MatchingSequenceBuilder builder;
	//matchingSequence_ = builder.buildSequence(cfg.getValueFromKey<std::string>("imagematching", ""));
}

ImageMatcher::~ImageMatcher()
{
}

float ImageMatcher::matchImages(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger) {
	DEB("Matching images ...");
	return matchingSequence_->executeSequence(refImg, testImg, refFrameN, testFrameN, logger, *cfg_);
}
} // namespace image
} // namespace eurofins