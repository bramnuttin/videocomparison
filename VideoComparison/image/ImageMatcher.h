#pragma once
#include <memory>
#include <opencv2/core/core.hpp>
#include "IAlgorithm.h"
#include "MatchingSequence.h"
#include "../utils/config/Config.h"

namespace eurofins {
namespace image {
class ImageMatcher
{
public:
	ImageMatcher(utils::Config& cfg);
	~ImageMatcher();

	float ImageMatcher::matchImages(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger);
private:
	std::shared_ptr<MatchingSequence> matchingSequence_;
	std::shared_ptr<utils::Config> cfg_;
};
} // namespace image
} // namespace eurofins
