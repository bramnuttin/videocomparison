#pragma once
#include <memory>
#include "MatchingSequence.h"
#include "../utils/config/TreeNode.h"
#include "../utils/config/Config.h"

namespace eurofins {
namespace image {
class SequenceFactory
{
public:
	SequenceFactory();
	~SequenceFactory();

	static std::shared_ptr<MatchingSequence> makeSequence(const utils::TreeNode<utils::AlgoString>* algoStringTree);
	static std::shared_ptr<IAlgorithm> makeAlgorithmFromAlgoString(const std::shared_ptr<utils::AlgoString> algoString);
	static void resolveTree(const utils::TreeNode<utils::AlgoString>* stringNode, utils::TreeNode<IAlgorithm>* node);
	static std::shared_ptr<IAlgorithm> makeAlgorithm(std::string& name, std::shared_ptr<std::map<std::string, std::string>> params);
};
} // namespace image
} // namespace eurofins
