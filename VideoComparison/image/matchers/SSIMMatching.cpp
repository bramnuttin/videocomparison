#include "SSIMMatching.h"
#include <iostream>
#include <opencv2\imgproc\imgproc.hpp>

namespace eurofins {
namespace image {
using utils::Logger;
using utils::LoggingInfo;
using utils::Log;

SSIMMatching::SSIMMatching(std::string params)
{
	std::shared_ptr<std::map<std::string, std::string>> paramsMap = utils::SequenceTextParser::getParamsFromString(params);
	setParams(paramsMap);
}

SSIMMatching::SSIMMatching(std::shared_ptr<std::map<std::string, std::string>> params)
{
	setParams(params);
}

SSIMMatching::~SSIMMatching()
{
}

float SSIMMatching::execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger) {
	cv::Scalar scalar = getMSSIM(refImg, testImg);
	auto value = scalar.val;

	DEB("SSIM(" << refFrameN << "," << testFrameN << "): " << scalar);

	if (*value > matchThreshold_) {
		LoggingInfo info(refFrameN, testFrameN, Log::match); //refFrameN, testFrameN
		info.similarity = *value;
		logger.log(info);
		return 1;
	} else {
		if (IAlgorithm::isBlack(testImg, 20)) {
			LoggingInfo info(refFrameN, testFrameN, Log::black); //refFrameN, testFrameN
			logger.log(info);
		} else {
			LoggingInfo info(refFrameN, testFrameN, Log::nomatch); //refFrameN, testFrameN
			info.similarity = *value;
			logger.log(info);
		}

		return 0;
	}

	//return (float) *(scalar.val); //TODO !!!!!
}

float SSIMMatching::execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger, utils::Config& config) {
	cv::Scalar scalar = getMSSIM(refImg, testImg);
	auto value = scalar.val;

	DEB("SSIM(" << refFrameN << "," << testFrameN << "): " << scalar);

	if (*value > matchThreshold_) {
		LoggingInfo info(refFrameN, testFrameN, Log::match); //refFrameN, testFrameN
		info.similarity = *value;
		logger.log(info);
		return 1;
	} else {
		if (IAlgorithm::isBlack(testImg, config.getValueFromKey<int>("blackthreshold", 100))) {
			LoggingInfo info(refFrameN, testFrameN, Log::black); //refFrameN, testFrameN
			logger.log(info);
		} else {
			LoggingInfo info(refFrameN, testFrameN, Log::nomatch); //refFrameN, testFrameN
			info.similarity = *value;
			logger.log(info);
		}

		return 0;
	}
}

cv::Scalar SSIMMatching::getMSSIM(const cv::Mat& i1, const cv::Mat& i2)
{
	const double C1 = 6.5025, C2 = 58.5225;
	/***************************** INITS **********************************/
	int d = CV_32F;

	cv::Mat I1, I2;
	i1.convertTo(I1, d);            // cannot calculate on one byte large values
	i2.convertTo(I2, d);

	cv::Mat I2_2 = I2.mul(I2);        // I2^2
	cv::Mat I1_2 = I1.mul(I1);        // I1^2
	cv::Mat I1_I2 = I1.mul(I2);        // I1 * I2

										/*************************** END INITS **********************************/

	cv::Mat mu1, mu2;                   // PRELIMINARY COMPUTING
	cv::GaussianBlur(I1, mu1, cv::Size(11, 11), 1.5);
	cv::GaussianBlur(I2, mu2, cv::Size(11, 11), 1.5);

	cv::Mat mu1_2 = mu1.mul(mu1);
	cv::Mat mu2_2 = mu2.mul(mu2);
	cv::Mat mu1_mu2 = mu1.mul(mu2);

	cv::Mat sigma1_2, sigma2_2, sigma12;

	cv::GaussianBlur(I1_2, sigma1_2, cv::Size(11, 11), 1.5);
	sigma1_2 -= mu1_2;

	cv::GaussianBlur(I2_2, sigma2_2, cv::Size(11, 11), 1.5);
	sigma2_2 -= mu2_2;

	cv::GaussianBlur(I1_I2, sigma12, cv::Size(11, 11), 1.5);
	sigma12 -= mu1_mu2;

	///////////////////////////////// FORMULA ////////////////////////////////
	cv::Mat t1, t2, t3;

	t1 = 2 * mu1_mu2 + C1;
	t2 = 2 * sigma12 + C2;
	t3 = t1.mul(t2);                 // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

	t1 = mu1_2 + mu2_2 + C1;
	t2 = sigma1_2 + sigma2_2 + C2;
	t1 = t1.mul(t2);                 // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

	cv::Mat ssim_map;
	divide(t3, t1, ssim_map);        // ssim_map =  t3./t1;

	cv::Scalar mssim = mean(ssim_map);   // mssim = average of ssim map
	return mssim;
}

void SSIMMatching::setParams(std::shared_ptr<std::map<std::string, std::string>> params) {
	for (auto iterator = params->begin(); iterator != params->end(); iterator++) {
		if (iterator->first == "threshold") {
			matchThreshold_ = std::stof(iterator->second);
		}
	}
}
} // namespace image
} // namespace eurofins