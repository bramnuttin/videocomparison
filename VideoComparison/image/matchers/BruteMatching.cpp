#include "BruteMatching.h"
#include <iostream>
#include "../../utils/definitions.h"

namespace eurofins {
namespace image {
using utils::Logger;
using utils::LoggingInfo;
using utils::Log;
BruteMatching::BruteMatching()
{
}

BruteMatching::~BruteMatching()
{
}

float BruteMatching::execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger) {
	uchar **refArray = new uchar*[refImg.rows*refImg.cols];
	uchar **testArray = new uchar*[testImg.rows*testImg.cols];
	if (refImg.isContinuous() && testImg.isContinuous()) {
		*refArray = refImg.data;
		*testArray = testImg.data;
	} else {
		return 0;
	}

	bool same = true;
	for (int i = 0; i < refImg.rows*testImg.cols; i++) {
		if (((int)(*refArray)[i]) != ((int)(*testArray)[i])) {
			same = false;
		} else {
		}
	}
	DEB("Brute(" << refFrameN << "," << testFrameN << "): " << same);

	if (same) {
		LoggingInfo info(refFrameN, testFrameN, Log::match); //refFrameN,
		info.similarity = same;
		logger.log(info);
	}

	else {
		LoggingInfo info(refFrameN, testFrameN, Log::nomatch); //refFrameN,

		info.log = Log::nomatch;
		logger.log(info);
	}

	return (float)same;
}
} // namespace image
} // namespace eurofins