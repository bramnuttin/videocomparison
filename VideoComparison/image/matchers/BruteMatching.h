#pragma once
#include "../IAlgorithm.h"

namespace eurofins {
namespace image {
class BruteMatching : public IAlgorithm
{
public:
	BruteMatching();
	~BruteMatching();

	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger);
};
} // namespace image
} // namespace eurofins
