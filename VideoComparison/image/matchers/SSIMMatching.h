#pragma once
#include "../IAlgorithm.h"
#include <opencv2\core\core.hpp>
#include "../../utils/config/Config.h"

namespace eurofins {
namespace image {
class SSIMMatching : public IAlgorithm
{
public:
	SSIMMatching();
	SSIMMatching(std::string params);
	SSIMMatching(std::shared_ptr<std::map<std::string, std::string>> params);
	~SSIMMatching();
	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger);
	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger, utils::Config& config);
	virtual void setParams(std::shared_ptr<std::map<std::string, std::string>> params);

private:
	float matchThreshold_ = 0.80f;

	cv::Scalar getMSSIM(const cv::Mat& i1, const cv::Mat& i2);
};
} // namespace image
} // namespace eurofins
