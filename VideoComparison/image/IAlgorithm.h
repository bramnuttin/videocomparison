#pragma once
#include <opencv2/core.hpp>
#include "../utils/logging/Logger.h"
#include "../utils/config/Config.h"
#include "../utils/config/SequenceTextParser.h"
#include "../utils/definitions.h"

namespace eurofins {
namespace image {
class IAlgorithm
{
public:
	virtual ~IAlgorithm() {}
	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger) = 0;
	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger, utils::Config& config) {
		return execute(refImg, testImg, refFrameN, testFrameN, logger);
	}

	virtual bool isBlack(const cv::Mat& img, const float threshold) const {
		int area = img.rows*img.cols;
		int channels = img.channels();
		float rSum = 0;
		float gSum = 0;
		float bSum = 0;
		float *channelSums = new float[channels];
		for (int i = 0; i < channels; i++) {
			channelSums[i] = 0;
		}
		for (int i = 0; i < img.rows; i++) {
			for (int j = 0; j < img.cols; j++) {
				for (int m = 0; m < channels; m++) {
					switch (channels) {
					case 1:
						channelSums[m] += img.at<cv::Vec<uchar, 1>>(i, j)[m];
						break;
					case 2:
						channelSums[m] += img.at<cv::Vec<uchar, 2>>(i, j)[m];
						break;
					case 3:
						channelSums[m] += img.at<cv::Vec<uchar, 3>>(i, j)[m];
						break;
					}
				}
			}
		}
		for (int i = 0; i < channels; i++) {
			if (channelSums[i] / area > threshold) {
				delete[] channelSums;
				return false;
			}
		}
		//std::cout << " ... BLACK ... " << std::endl;
		//system("PAUSE");
		delete[] channelSums;
		return true;
	}
};
} // namespace image
} // namespace eurofins
