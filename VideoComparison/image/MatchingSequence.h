#pragma once
#include "../utils/config/TreeNode.h"
#include "IAlgorithm.h"

namespace eurofins {
namespace image {
class MatchingSequence
{
public:
	MatchingSequence(utils::TreeNode<IAlgorithm>* tree);
	~MatchingSequence();

	float executeSequence(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger, utils::Config& config) const;
private:
	float execute(const utils::TreeNode<IAlgorithm>* tree, cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger, utils::Config& config) const;
	utils::TreeNode<IAlgorithm>* tree_;
};
} // namespace image
} // namespace eurofins
