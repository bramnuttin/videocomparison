#pragma once
#include "../IAlgorithm.h"

namespace eurofins {
namespace image {
class CannyEdge : public IAlgorithm
{
public:
	CannyEdge();
	CannyEdge(std::string params);
	CannyEdge(std::shared_ptr<std::map<std::string, std::string>> params);
	~CannyEdge();

	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger);
	virtual void setParams(std::shared_ptr<std::map<std::string, std::string>> params);

private:
	float minValue_ = 100;
	float maxValue_ = 200;
	float apertureSize_ = 3;
	bool L2Gradient_ = false;
};
} // namespace image
} // namespace eurofins
