#pragma once
#include "../IAlgorithm.h"

namespace eurofins {
namespace image {
class BlackWhiteModifier : public IAlgorithm
{
public:
	BlackWhiteModifier();
	~BlackWhiteModifier();

	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger);
};
} // namespace image
} // namespace eurofins
