#include "BlackWhiteModifier.h"
#include <opencv2\core\core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

namespace eurofins {
namespace image {
using utils::Logger;
BlackWhiteModifier::BlackWhiteModifier()
{
}

BlackWhiteModifier::~BlackWhiteModifier()
{
}

float BlackWhiteModifier::execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger)
{
	//cv::Mat *mats = new cv::Mat;
	//cv::Mat ref_gray;
	cv::cvtColor(refImg, refImg, CV_RGB2GRAY);
	refImg = refImg > 128;

	//cv::Mat test_gray;
	cv::cvtColor(testImg, testImg, CV_RGB2GRAY);
	testImg = testImg > 128;

	//mats[0] = ref_bw;
	//mats[1] = test_bw;
	//return mats;
	return -1;
}
} // namespace image
} // namespace eurofins