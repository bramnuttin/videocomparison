#pragma once
#include "../IAlgorithm.h"
#include <string>
namespace eurofins {
namespace image {
class Pixelater : public IAlgorithm
{
public:
	Pixelater();
	Pixelater(std::string params);
	Pixelater(std::shared_ptr<std::map<std::string, std::string>> params);
	~Pixelater();

	float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger) override;
	virtual void setParams(std::shared_ptr<std::map<std::string, std::string>> params);

private:
	int density_ = 1;
	//which frame to target (0 = ref, 1 = test, 2 = both)
	int target_ = 2;
};
} // namespace eurofins
} // namespace image
