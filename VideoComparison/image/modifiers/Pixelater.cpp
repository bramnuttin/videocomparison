#include "Pixelater.h"
#include <opencv2\xfeatures2d.hpp>
#include <opencv2\imgproc.hpp>
#include <map>
namespace eurofins {
namespace image {
Pixelater::Pixelater()
{
}

Pixelater::Pixelater(std::string params)
{
	std::shared_ptr<std::map<std::string, std::string>> paramsMap = utils::SequenceTextParser::getParamsFromString(params);
	setParams(paramsMap);
}

Pixelater::Pixelater(std::shared_ptr<std::map<std::string, std::string>> params)
{
	setParams(params);
}

Pixelater::~Pixelater()
{
}

float Pixelater::execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger) {
	if (target_ == 0 || target_ == 2) {
		cv::Size refS(refImg.cols, refImg.rows);

		refImg.convertTo(refImg, -1, 1.1, 0);

		cv::resize(refImg, refImg, (refS*density_) / 100, 1, 1, cv::INTER_NEAREST);
		cv::resize(refImg, refImg, refS, 1, 1, cv::INTER_NEAREST);
	}

	if (target_ == 1 || target_ == 2) {
		cv::Size testS(testImg.cols, testImg.rows);
		testImg.convertTo(testImg, -1, 1.1, 0);

		cv::resize(testImg, testImg, (testS*density_) / 100, 1, 1, cv::INTER_NEAREST);
		cv::resize(testImg, testImg, testS, 1, 1, cv::INTER_NEAREST);
	}
	return -1;
}

void Pixelater::setParams(std::shared_ptr<std::map<std::string, std::string>> params) {
	for (auto iterator = params->begin(); iterator != params->end(); iterator++) {
		if (iterator->first == "density") {
			density_ = std::stoi(iterator->second);
		} else if (iterator->first == "target") {
			target_ = std::stoi(iterator->second);
		}
	}
}
} // namespace eurofins
} // namespace image