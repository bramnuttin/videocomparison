#pragma once
#include "../IAlgorithm.h"

namespace eurofins {
namespace image {
class Resizer : public IAlgorithm
{
public:
	Resizer();
	Resizer(std::string params);
	Resizer(std::shared_ptr<std::map<std::string, std::string>> params);
	~Resizer();

	virtual float execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, utils::Logger& logger);
	virtual void setParams(std::shared_ptr<std::map<std::string, std::string>> params);

private:
	float factor_ = 1.0f;
	//which frame to target (0 = ref, 1 = test, 2 = let algorithm decide (smallest area))
	int target_ = 2;
};
} // namespace image
} // namespace eurofins
