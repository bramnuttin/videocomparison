#include "Resizer.h"
#include <opencv2\xfeatures2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <map>
#include <iostream>

namespace eurofins {
namespace image {
using utils::Logger;
Resizer::Resizer()
{
}

Resizer::Resizer(std::string params)
{
	std::shared_ptr<std::map<std::string, std::string>> paramsMap = utils::SequenceTextParser::getParamsFromString(params);
	setParams(paramsMap);
}

Resizer::Resizer(std::shared_ptr<std::map<std::string, std::string>> params)
{
	setParams(params);
}

Resizer::~Resizer()
{
}

float Resizer::execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger)
{
	//auto size = refImg.size() * ((int)(factor*10) /10);
	//size = refImg.size() < testImg.size() ? refImg.size() : testImg.size()
	cv::Size size;
	if (target_ == 0) {
		size = refImg.size();
	} else if (target_ == 1) {
		size = testImg.size();
	} else {
		size = refImg.size().area() < testImg.size().area() ? refImg.size() : testImg.size();
	}

	size.width *= factor_;
	size.height *= factor_;
	resize(refImg, refImg, size, 0, 0, cv::INTER_LANCZOS4);
	resize(testImg, testImg, size, 0, 0, cv::INTER_LANCZOS4);

	return -1;
}

void Resizer::setParams(std::shared_ptr<std::map<std::string, std::string>> params)
{
	for (auto iterator = params->begin(); iterator != params->end(); iterator++) {
		if (iterator->first == "factor") {
			factor_ = std::stof(iterator->second);
		} else if (iterator->first == "target") {
			target_ = std::stoi(iterator->second);
		}
	}
}
} // namespace image
} // namespace eurofins