#include "CannyEdge.h"
#include <opencv2\core\core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <map>
namespace eurofins {
namespace image {
using utils::Logger;
CannyEdge::CannyEdge()
{
}

CannyEdge::CannyEdge(std::string params)
{
	std::shared_ptr<std::map<std::string, std::string>> paramsMap = utils::SequenceTextParser::getParamsFromString(params);
	setParams(paramsMap);
}

CannyEdge::CannyEdge(std::shared_ptr<std::map<std::string, std::string>> params)
{
	setParams(params);
}

CannyEdge::~CannyEdge()
{
}

float CannyEdge::execute(cv::Mat& refImg, cv::Mat& testImg, const int refFrameN, const int testFrameN, Logger& logger)
{
	cv::Mat refOut;
	cv::Mat testOut;
	cv::Canny(refImg, refOut, minValue_, maxValue_, apertureSize_, L2Gradient_);
	cv::Canny(testImg, testOut, minValue_, maxValue_, apertureSize_, L2Gradient_);
	refImg = refOut;
	testImg = testOut;

	return -1;
}

void CannyEdge::setParams(std::shared_ptr<std::map<std::string, std::string>> params)
{
	for (auto iterator = params->begin(); iterator != params->end(); iterator++) {
		// iterator->first = key
		// iterator->second = value
		// Repeat if you also want to iterate through the second map.
		if (iterator->first == "minVal") {
			minValue_ = std::stof(iterator->second);
		} else if (iterator->first == "maxVal") {
			maxValue_ = std::stof(iterator->second);
		} else if (iterator->first == "apertureSize") {
			apertureSize_ = std::stof(iterator->second);
		} else if (iterator->first == "L2Gradient") {
			auto L2GradientText = iterator->second;
			if (L2GradientText == "true") {
				L2Gradient_ = true;
			} else {
				L2Gradient_ = false;
			}
		}
	}
}
} // namespace image
} // namespace eurofins