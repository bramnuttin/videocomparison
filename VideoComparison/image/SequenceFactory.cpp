#include "SequenceFactory.h"
#include <memory>
#include "matchers/BruteMatching.h"
#include "matchers/SSIMMatching.h"
#include "modifiers/BlackWhiteModifier.h"
#include "modifiers/CannyEdge.h"
#include "modifiers/Pixelater.h"
#include "modifiers/Resizer.h"
#include "../utils/config/AlgoStringSequenceBuilder.h"

namespace eurofins {
namespace image {
std::shared_ptr<MatchingSequence> SequenceFactory::makeSequence(const utils::TreeNode<utils::AlgoString>* algoStringTree)
{
	auto algoTree = utils::AlgoStringSequenceBuilder::convertTree2T<IAlgorithm>(algoStringTree, &makeAlgorithmFromAlgoString);
	return std::make_shared<MatchingSequence>(algoTree);
}

void SequenceFactory::resolveTree(const utils::TreeNode<utils::AlgoString>* stringNode, utils::TreeNode<IAlgorithm>* parent) {
	auto current = new utils::TreeNode<IAlgorithm>(makeAlgorithmFromAlgoString(stringNode->algo), stringNode->threshold);

	if (!stringNode->children->empty()) {
		for (int i = 0; i < stringNode->children->size(); i++) {
			resolveTree(stringNode->children->at(i), current);
		}
	}
	parent->addChildNode(current);
}

std::shared_ptr<IAlgorithm> SequenceFactory::makeAlgorithmFromAlgoString(const std::shared_ptr<utils::AlgoString> algoString) {
	std::string brutematching = "brutematching";
	std::string ssimmatching = "ssimmatching";
	std::string blackwhitemodifier = "blackwhitemodifier";
	std::string cannyedge = "cannyedge";
	std::string resizer = "resize";
	std::string pixelater = "pixelate";

	if (algoString->name.substr(0, brutematching.size()) == brutematching) {
		return std::make_shared<BruteMatching>();
	} else if (algoString->name.substr(0, ssimmatching.size()) == ssimmatching) {
		return std::make_shared<SSIMMatching>(algoString->params);
	} else if (algoString->name.substr(0, blackwhitemodifier.size()) == blackwhitemodifier) {
		return std::make_shared<BlackWhiteModifier>();
	} else if (algoString->name.substr(0, cannyedge.size()) == cannyedge) {
		return std::make_shared<CannyEdge>(algoString->params);
	} else if (algoString->name.substr(0, resizer.size()) == resizer) {
		return std::make_shared<Resizer>(algoString->params);
	} else if (algoString->name.substr(0, pixelater.size()) == pixelater) {
		return std::make_shared<Pixelater>(algoString->params);
	}
}

std::shared_ptr<IAlgorithm> SequenceFactory::makeAlgorithm(std::string& name, std::shared_ptr<std::map<std::string, std::string>> params) {
	std::string brutematching = "brutematching";
	std::string ssimmatching = "ssimmatching";
	std::string blackwhitemodifier = "blackwhitemodifier";
	std::string cannyedge = "cannyedge";
	std::string resizer = "resize";
	std::string pixelater = "pixelate";

	if (name.substr(0, brutematching.size()) == brutematching) {
		return std::make_shared<BruteMatching>();
	} else if (name.substr(0, ssimmatching.size()) == ssimmatching) {
		return std::make_shared<SSIMMatching>(params);
	} else if (name.substr(0, blackwhitemodifier.size()) == blackwhitemodifier) {
		return std::make_shared<BlackWhiteModifier>();
	} else if (name.substr(0, cannyedge.size()) == cannyedge) {
		return std::make_shared<CannyEdge>(params);
	} else if (name.substr(0, resizer.size()) == resizer) {
		return std::make_shared<Resizer>(params);
	} else if (name.substr(0, pixelater.size()) == pixelater) {
		return std::make_shared<Pixelater>(params);
	}
}
} // namespace image
} // namespace eurofins