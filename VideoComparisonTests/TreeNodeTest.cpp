#include "stdafx.h"
#include "CppUnitTest.h"

using namespace eurofins::utils;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace VideoComparisonTests
{
TEST_CLASS(TreeNodeTest)
{
public:

	TEST_METHOD(NoChildrenInRootOnlyTree)
	{
		float threshold[2] = { 0.1f, 0.2f };
		TreeNode<int> tree(std::make_shared<int>(1), threshold);
		Assert::IsTrue(tree.children->empty());
	}
};
} // VideoComparisonTests