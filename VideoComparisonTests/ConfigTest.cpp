#include "stdafx.h"
#include "CppUnitTest.h"
#include <istream>
#include <Windows.ApplicationModel.resources.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace eurofins::utils;

namespace VideoComparisonTests
{
TEST_CLASS(ConfigTest)
{
public:

	TEST_METHOD(LoadSettings)
	{
		auto loadSettings = [this] { Config::loadSettings(""); };
		Assert::ExpectException<ConfigException>(loadSettings);

		auto settings = Config::loadSettings("configtest.cfg"); //wrong path
		Assert::AreEqual(settings->getValueFromKey<>(std::string("testkey"), std::string("")), std::string("testvalue"));

		// TODO sequence testing
	}

	TEST_METHOD(GetValueFromKey)
	{
		auto settings = Config::loadSettings("configtest.cfg"); //wrong path
		Assert::AreEqual(settings->getValueFromKey<>(std::string("testkey"), std::string("")), std::string("testvalue"));
		Assert::AreEqual(settings->getValueFromKey<>(std::string("doesntexist"), std::string("notfound")), std::string("notfound"));
	}
};
} // VideoComparisonTests