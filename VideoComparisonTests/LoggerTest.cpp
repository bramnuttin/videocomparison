#include "stdafx.h"
#include "CppUnitTest.h"

using namespace eurofins::utils;
namespace cutf = Microsoft::VisualStudio::CppUnitTestFramework;

namespace VideoComparisonTests
{
TEST_CLASS(LoggerTest)
{
public:

	TEST_METHOD(LoggerConstructor)
	{
		Logger logger;

		cutf::Assert::IsNotNull(&logger);
	}
	TEST_METHOD(LoggingInfoInsertionOperator)
	{
		LoggingInfo info(0, 0, Log::black);
		LoggingInfo info2(0, 0, Log::freeze);
		info.similarity = 1;
		info2.similarity = 0.5;
		info << info2;
		cutf::Assert::IsTrue(info2.log == info.log && info2.similarity == info.similarity);
	}
	TEST_METHOD(ProcessLogsTest)
	{
		//Should be done in Logger
		/*Logger logger;
		logger.log(LoggingInfo(0, 0, Log::black));
		logger.log(LoggingInfo(0, 1, Log::match));
		logger.log(LoggingInfo(0, 2, Log::nomatch));
		logger.log(LoggingInfo(0, 3, Log::match));
		logger.log(LoggingInfo(1, 0, Log::black));
		logger.log(LoggingInfo(2, 0, Log::black));
		logger.log(LoggingInfo(3, 0, Log::black));
		logger.log(LoggingInfo(1, 1, Log::nomatch));
		logger.log(LoggingInfo(1, 2, Log::match));
		logger.log(LoggingInfo(1, 3, Log::nomatch));
		logger.log(LoggingInfo(2, 1, Log::nomatch));
		logger.log(LoggingInfo(3, 1, Log::match));
		logger.log(LoggingInfo(2, 2, Log::nomatch));
		logger.log(LoggingInfo(2, 3, Log::nomatch));
		logger.log(LoggingInfo(3, 2, Log::nomatch));
		logger.log(LoggingInfo(3, 3, Log::match));

		logger.outputCLI();
		system("PAUSE");*/
		//cutf::Assert::IsTrue(info2.log == info.log && info2.similarity == info.similarity);
	}
};
} // VideoComparisonTests