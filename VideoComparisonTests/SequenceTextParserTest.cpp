#include "stdafx.h"
#include "CppUnitTest.h"

using namespace eurofins::utils;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace VideoComparisonTests
{
TEST_CLASS(SequenceTextParserTest)
{
public:

	TEST_METHOD(GetCurrentNodeValue)
	{
		auto result = seqBuilder.getCurrentNodeValue("[testest[uhu]]");
		Assert::AreEqual(result, std::string("testest"));
	}
	TEST_METHOD(GetChildNodes)
	{
		auto result = seqBuilder.getChildNodes("[testest[uhu]]");
		Assert::AreEqual((int)result->size(), 1);
		Assert::AreEqual(result->at(0), std::string("[uhu]"));
		result = seqBuilder.getChildNodes("[testest[uhu][aha]]");
		Assert::AreEqual((int)result->size(), 2);
		Assert::AreEqual(result->at(0), std::string("[uhu]"));
		Assert::AreEqual(result->at(1), std::string("[aha]"));
		result = seqBuilder.getChildNodes("[testest[uhu[]][]]");
		Assert::AreEqual((int)result->size(), 2);
		Assert::AreEqual(result->at(0), std::string("[uhu[]]"));
		Assert::AreEqual(result->at(1), std::string("[]"));
	}
	TEST_METHOD(GetNodeParams)
	{
		auto result = seqBuilder.getNodeParams("[testest(a,b)]");
		Assert::AreEqual(result, std::string("a,b"));
	}
private:
	SequenceTextParser seqBuilder;
};
} // VideoComparisonTests